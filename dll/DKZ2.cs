﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO.Ports;
using NLog;

namespace SPI_AOI.Devices.Light
{
    public class DKZ2
    {
        private Logger mLog = Heal.LogCtl.GetInstance();
        public SerialPort Serial { get; set; }
        private static DKZ2 DKZ = null;
        private static Object lockObj = new Object();
        public static DKZ2 GetInstance()
        {
            lock (lockObj)
            {
                if (DKZ == null)
                {
                    DKZ = new DKZ2();
                    DKZ.Serial = new SerialPort();
                    DKZ.Serial.BaudRate = 19200;
                }
            }
            return DKZ;
        }
        public bool IsOpen
        {
            get 
            { 
                return Serial.IsOpen; 
            }
        }
        public int Connect(string PortName)
        {
            if(Serial == null)
            {
                Serial = new SerialPort();
                Serial.PortName = PortName;
                Serial.BaudRate = 19200;
            }
            if(Serial.IsOpen)
            {
                Serial.Close();
            }
            try
            {
                Serial.PortName = PortName;
                Serial.WriteTimeout = 2000;
                Serial.ReadTimeout = 2000;
                Serial.Open();
            }
            catch
            {
                return -1;
            }
            return 0;
        }
        public int Disconnect()
        {
            if (Serial.IsOpen)
            {
                try
                {
                    Serial.Close();
                }
                catch
                {
                    return -1;
                }
                return 0;
            }
            return 0;
        }
        public int ActiveOne(int Channel, int Mode)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x03, 0x32, 0x00, 0x00 };
            data[4] = Convert.ToByte(Channel - 1);
            data[5] = Convert.ToByte(Mode);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                Thread.Sleep(10);
                int response = Serial.ReadByte();
                if (response == 0x4f || response == 0x6f)
                    succ = true;
                Serial.DiscardInBuffer();
                Serial.DiscardOutBuffer();
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int SetOne(int Channel, int Value)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x03, 0x31, 0x00, 0x00 };
            data[4] = Convert.ToByte(Channel - 1);
            data[5] = Convert.ToByte(Value);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                Thread.Sleep(10);
                int response = Serial.ReadByte();
                if (response == 0x4f || response == 0x6f)
                    succ = true;
                Serial.DiscardInBuffer();
                Serial.DiscardOutBuffer();
            }
            catch (Exception ex)
            {
                mLog.Error(ex.Message);
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int SetFour(int Value1, int Value2, int Value3, int Value4)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x33, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Value1);
            data[5] = Convert.ToByte(Value2);
            data[6] = Convert.ToByte(Value3);
            data[7] = Convert.ToByte(Value4);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                Thread.Sleep(10);
                int response = Serial.ReadByte();
                if (response == 0x4f || response == 0x6f)
                    succ = true;
                Serial.DiscardInBuffer();
                Serial.DiscardOutBuffer();
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int SetFour(int[] Value)
        {
            ActiveFour(1, 1, 1, 1);
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x33, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Value[0]);
            data[5] = Convert.ToByte(Value[1]);
            data[6] = Convert.ToByte(Value[2]);
            data[7] = Convert.ToByte(Value[3]);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                Thread.Sleep(10);
                int response = Serial.ReadByte();
                if (response == 0x4f || response == 0x6f)
                    succ = true;
                Serial.DiscardInBuffer();
                Serial.DiscardOutBuffer();
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int ActiveFour(int Mode1, int Mode2, int Mode3, int Mode4)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x34, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Mode1);
            data[5] = Convert.ToByte(Mode2);
            data[6] = Convert.ToByte(Mode3);
            data[7] = Convert.ToByte(Mode4);
            bool succ = false;
            try
            {
                Serial.Write(data, 0, data.Length);
                Thread.Sleep(10);
                int response = Serial.ReadByte();
                if (response == 0x4f || response == 0x6f)
                    succ = true;
                Serial.DiscardInBuffer();
                Serial.DiscardOutBuffer();
            }
            catch
            {
                return -1;
            }
            if (succ)
                return 0;
            else
                return -2;
        }
        public int[] GetStatus()
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x02, 0x02, 0x0d };
            bool succ = false;
            int[] result = new int[8];
            try
            {
                Serial.Write(data, 0, data.Length);
                Thread.Sleep(10);
                byte[] response = new byte[12];
                Serial.Read(response, 0, response.Length);
                for (int i = 0; i < 8; i++)
                {
                    result[i] = Convert.ToInt32(response[i + 4]);
                }
                succ = true;
            }
            catch
            {
                return null;
            }
            if (succ)
                return result;
            else
                return null;
        }
    }
}
