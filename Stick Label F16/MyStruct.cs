﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16
{
    public enum Status
    {
        Fail,
        OK,
        R_OutRange,
        X_OutRange,
        Y_OutRange,
    }
}
