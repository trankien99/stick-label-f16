﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Stick_Label_F16.Algorithm.Properties;
using Newtonsoft.Json;
using System.Drawing;
using System.IO;
using Emgu.CV.Util;
using Stick_Label_F16.Config;
namespace Stick_Label_F16.Algorithm.Inspection
{
    public class Scratch:IProcessing
    {
        Properties.Scratch _Properties = null;
        private string ReadCode(Image<Bgr, byte> imgCode)
        {
            CvInvoke.Imwrite("code.png", imgCode);
            var result = Service.ReadCode.Decode(new string[] { "code.png" }, false);
            File.Delete("code.png");
            string sn = null;
            if (result != null)
                sn = result.SN;
            return sn;
        }
        private Mat Close(Mat img, int horizontalValue =1,int verticalValue=1)
        {
            Mat img_closed = new Mat();
            Mat kernel = CvInvoke.GetStructuringElement(ElementShape.Rectangle,
                new System.Drawing.Size(horizontalValue, verticalValue),
                new System.Drawing.Point(-1, -1));
            CvInvoke.MorphologyEx(img, img_closed, MorphOp.Close, kernel, new System.Drawing.Point(-1, -1), 1, BorderType.Default, CvInvoke.MorphologyDefaultBorderValue);
            return img_closed;
        }
        private VectorOfVectorOfPoint ReturnAllContours(Mat img)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Emgu.CV.CvEnum.RetrType type = Emgu.CV.CvEnum.RetrType.List;
            Emgu.CV.CvEnum.ChainApproxMethod method = Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple;
            CvInvoke.FindContours(img, contours, null, type, method);
            return contours;
        }
        private bool IsScratch(Mat img)
        {
            Mat imgGray = new Mat();
            CvInvoke.CvtColor(img, imgGray, ColorConversion.Bgr2Gray);
            Mat imgThreshold = new Mat();
            CvInvoke.Threshold(imgGray, imgThreshold, 127, 255, ThresholdType.BinaryInv);
            Mat imgClosed = Close(imgThreshold, horizontalValue: 8);
            VectorOfVectorOfPoint contours = ReturnAllContours(imgClosed);
            if (contours.Size == 1)
                return true;
            else
                return false;
        }
        public List<bool> Inspect(Bitmap img, List<IProperty> properties,string snPrinted, ResultLocation offsetValue)
        {
            List<bool> listResult = new List<bool>();
            using (Image<Bgr, byte> image = new Image<Bgr, byte>(img))
            {
                for (int i=0;i< properties.Count; i++)
                {
                    string js = JsonConvert.SerializeObject(properties[i]);
                    _Properties = JsonConvert.DeserializeObject<Properties.Scratch>(js);
                    image.ROI = _Properties.ROI;
                    string sn = ReadCode(image);
                    if (sn == snPrinted)
                    {
                        bool resScratcd = IsScratch(image.Mat);
                        if (resScratcd)
                            listResult.Add(true);
                        else
                            listResult.Add(false);
                    }
                    else
                        listResult.Add(false);
                }
            }
            return listResult;
        }
        public ResultLocation Locate(Image<Bgr, byte> img,IProperty property, Point samplePoint,double angle, Trigger trigger)
        {
            ResultLocation result = new ResultLocation();

            return result;
        }
    }
}
