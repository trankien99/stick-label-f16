﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Newtonsoft.Json;
using Stick_Label_F16.Algorithm.Properties;
using Stick_Label_F16.Config;
namespace Stick_Label_F16.Algorithm.Inspection
{
    public class Eccentric:IProcessing
    {
        private Properties.Eccentric _Properties = new Properties.Eccentric();
        private delegate VectorOfPoint MaxContourDelegate(Mat img);
        private Stick_Label_F16.Properties.Settings _Param = Stick_Label_F16.Properties.Settings.Default;
        private VectorOfVectorOfPoint ReturnAllContours(Mat img)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(img, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
            return contours;
        }
        private VectorOfPoint ReturnMaxContour(Mat img)
        {
            Mat imgThreshold = new Mat();
            CvInvoke.Threshold(img, imgThreshold, 127, 255, Emgu.CV.CvEnum.ThresholdType.BinaryInv);
            var contours = ReturnAllContours(imgThreshold);
            double max = 0;
            VectorOfPoint maxCnt = new VectorOfPoint();
            for (int i = 0; i < contours.Size; i++)
            {
                double x = CvInvoke.ContourArea(contours[i]);
                if (x > max)
                {
                    max = x;
                    maxCnt = contours[i];
                }
            }
            return maxCnt;
        }
        public List<bool> Inspect(Bitmap img,List<IProperty> properties,string snPrint,ResultLocation offsetValue)
        {
            List<bool> listResult = new List<bool>();
            using(Image<Bgr, byte>image = new Image<Bgr, byte>(img))
            {
                for(int i=0;i< properties.Count; i++)
                {
                    CvInvoke.Imwrite("imgInspection.jpg", image);
                    string js = JsonConvert.SerializeObject(properties[i]);
                    _Properties = JsonConvert.DeserializeObject<Properties.Eccentric>(js);
                    image.ROI = _Properties.ROI;
                    Mat imgThreshold = Algorithm.Locate.Threshold.ReturnImgThreshold(image.Mat, _Properties.Thresh,type:ThresholdType.BinaryInv);
                    Mat imgClosed = close(imgThreshold, _Properties.ValueClose);
                    VectorOfPoint maxCnt = Algorithm.Locate.Threshold.FindlargestContour(imgClosed);
                    var shape = CvInvoke.MinAreaRect(maxCnt);
                    double angle = Algorithm.Locate.Threshold.getAngleLocate(shape);
                    double deltaAngle = 0;
                    if (angle * _Properties.Angle > 0)
                        deltaAngle = Math.Abs(angle - _Properties.Angle);
                    else
                        deltaAngle = angle;
                    System.Drawing.PointF c = shape.Center;
                    Point centerPoint = new Point((int)c.X + _Properties.ROI.X, (int)c.Y + _Properties.ROI.Y);
                    double deltaX = Math.Abs(centerPoint.X - _Properties.SamplePoint.X + offsetValue.X_Offset);
                    double deltaY = Math.Abs(centerPoint.Y - _Properties.SamplePoint.Y + offsetValue.Y_Offset);
                    if (deltaX > _Param.TOLERANCE * _Param.PIXEL_MM_C3 || deltaY > _Param.TOLERANCE*_Param.PIXEL_MM_C3 || deltaAngle> _Param.ANGLE_TOLERANCE)
                        listResult.Add(false);
                    else
                        listResult.Add(true);
                   
                }  
            }
            return listResult;
        }
        public static Mat close(Mat img,int value)
        {
            if (value == 0)
                value = 1;
            Mat img_closed = new Mat();
            Mat kernel = CvInvoke.GetStructuringElement(ElementShape.Rectangle,
            new Size(value, 1),
            new Point(-1, -1));
            CvInvoke.MorphologyEx(img, img_closed, MorphOp.Close, kernel, new System.Drawing.Point(-1, -1), 1, BorderType.Default, CvInvoke.MorphologyDefaultBorderValue);
            return img_closed;
        }
        public static string ReadCode(Image<Bgr, byte> imgCode)
        {
            Mat imgGray = new Mat();
            CvInvoke.CvtColor(imgCode, imgGray, ColorConversion.Bgr2Gray);
            CvInvoke.Imwrite("code.png", imgCode);
            var result = Service.ReadCode.Decode(new string[] { "code.png" }, false);
            File.Delete("code.png");
            string sn = null;
            if (result != null)
                sn = result.SN;
            return sn;
        }
        public static string Decode(Image<Bgr, byte> img,double angle)
        {
            Point imgCenter = new Point((int)(img.Cols / 2), (int)(img.Rows / 2));
            var imgRotated = Algorithm.Locate.Threshold.RotateImg(img, imgCenter, angle);
            CvInvoke.Imwrite("imgClosed.jpg", imgRotated);
            string sn = ReadCode(new Image<Bgr, byte>(imgRotated.Bitmap));
            return sn;
        }
        public ResultLocation Locate(Image<Bgr, byte> img,IProperty property, Point samplePoint,double angle,Trigger trigger)
        {
            ResultLocation result = new ResultLocation();
            return result;
        }
    }
}
