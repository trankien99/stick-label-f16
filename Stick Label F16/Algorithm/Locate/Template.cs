﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using Stick_Label_F16.Algorithm.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stick_Label_F16.Config;

namespace Stick_Label_F16.Algorithm.Locate
{
    public class Template : IProcessing
    {
        private Properties.Template _Properties = null;
        public static MatchingResult BestTemplateMatching(Image<Bgr, byte> ImageSource, Image<Bgr, byte> ImageTemplate, Rectangle ROI, TemplateMatchingType MatchingType)
        {
            MatchingResult matchingResult = new MatchingResult();
            double bestScore = 0;
            Rectangle bestLocation = Rectangle.Empty;
            using (var image = ImageSource.Copy())
            {
                image.ROI = ROI;
                using (Mat result = new Mat())
                {
                    CvInvoke.MatchTemplate(image, ImageTemplate, result, MatchingType);
                    double minVal = 0;
                    double maxVal = 0;
                    Point minLoc = new Point();
                    Point maxLoc = new Point();
                    CvInvoke.MinMaxLoc(result, ref minVal, ref maxVal, ref minLoc, ref maxLoc);
                    bestLocation = new Rectangle(maxLoc, ImageTemplate.Size);
                    bestScore = maxVal;
                }
            }

            bestLocation.X += ROI.X;
            bestLocation.Y += ROI.Y;
            //var img_clone = ImageSource.Clone();
            //img_clone.ROI = bestLocation;
            //string path_save = $"{mParam.SAVE_PATH}/Template Box/ ";
            //if (!Directory.Exists(path_save))
            //{
            //    Directory.CreateDirectory(path_save);
            //}
            //string time = Convert.ToString(DateTime.Now).Split(' ')[1].Replace(':', '_');
            //string path = $"{path_save}templateBox{time}.jpg";
            //CvInvoke.Imwrite(path, img_clone);
            matchingResult.Location = bestLocation;
            matchingResult.Score = bestScore;
            return matchingResult;
        }

        public List<bool> Inspect(Bitmap img, List<IProperty> property, string snPrint, ResultLocation offsetValue)
        {
            throw new NotImplementedException();
        }

        public ResultLocation Locate(Image<Bgr, byte> img, IProperty property, Point samplePoint,double angle, Trigger trigger)
        {
            using(Image<Bgr,byte>image = new Image<Bgr, byte>(img.Bitmap))
            {
                CvInvoke.Imwrite("imgTemplate.jpg", image);
                ResultLocation result = new ResultLocation();
                string js = JsonConvert.SerializeObject(property);
                _Properties = JsonConvert.DeserializeObject<Properties.Template>(js);
                double smallest = 0.03;
                int inflateX = Convert.ToInt32(_Properties.SearchX / smallest);
                int inflateY = Convert.ToInt32(_Properties.SearchY / smallest);
                var r = new Rectangle(_Properties.ROI.X, _Properties.ROI.Y, _Properties.ROI.Width, _Properties.ROI.Height);
                r.Inflate(inflateX, inflateY);
                Image<Bgr, byte> template_img = new Image<Bgr, byte>(_Properties.PathTemplate);
                MatchingResult res = BestTemplateMatching(image, template_img, r, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed);
                if (res.Score*100 >= _Properties.MatchingScore)
                {
                    result.Status = Status.OK;
                    result.X_Offset = _Properties.ROI.X - res.Location.X;
                    result.Y_Offset = _Properties.ROI.Y - res.Location.Y;
                }
                else
                    result.Status = Status.Fail;
                return result;
            }  
        }

    }
    public class MatchingResult
    {
        public Rectangle Location { get; set; }
        public double Score { get; set; }
    }
}
