﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Stick_Label_F16.Algorithm.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stick_Label_F16.Config;
using Newtonsoft.Json;

namespace Stick_Label_F16.Algorithm.Locate
{
    public class Threshold:IProcessing
    {
        private Properties.Threshold _Properties = null;
        public static Mat ReturnImgThreshold(Mat img,int thresh, ThresholdType type = ThresholdType.Binary)
        {
            Mat img_gray = new Mat();
            CvInvoke.CvtColor(img, img_gray, ColorConversion.Bgr2Gray);
            Mat img_smooth = new Mat();
            CvInvoke.GaussianBlur(img_gray, img_smooth, new Size(5, 5), 0);
            Mat img_theshold = new Mat();
            CvInvoke.Threshold(img_smooth, img_theshold, thresh, 255, type);
            return img_theshold;
        }
        public static VectorOfPoint FindlargestContour(Mat thresh)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(thresh, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
            double max = 0;
            VectorOfPoint largest_cnt = new VectorOfPoint();
            if (contours.Size > 0)
            {
                for (int i = 0; i < contours.Size; i++)
                {
                    double s = CvInvoke.ContourArea(contours[i]);
                    if (s > max)
                    {
                        max = s;
                        largest_cnt = contours[i];
                    }
                }
                return largest_cnt;
            }
            else
                return null;

            //CvInvoke.DrawContours(img, largest_cnt, -1, new MCvScalar(0, 255, 0), 3);
            //string path_save = $"{mParam.SAVE_PATH}/Processing Image/ ";
            //if (!Directory.Exists(path_save))
            //{
            //    Directory.CreateDirectory(path_save);
            //}

            
        }
        public static RotatedRect getCenterContour(VectorOfPoint cnt)
        {
            RotatedRect O = new RotatedRect();
            try
            {
                O = CvInvoke.MinAreaRect(cnt);
               
            }
            catch
            {
                
            }
            return O;
        }
        public static PointF[] getPoints(RotatedRect area_rect)
        {
            PointF[] list_point = new PointF[] { };
            list_point = CvInvoke.BoxPoints(area_rect);
            return list_point;
        }
        public static double getAngleLocate(RotatedRect area_rect)
        {
            //thuan la am
            float angle_rotation = area_rect.Angle;
            if (angle_rotation <= -45)
            {
                return 90 + angle_rotation;
            }
            else
            {
                return angle_rotation;
            }
        }
        public static Point PointRotation(Point RotatePoint, Point Center, double Angle)
        {
            Point newPoint = new Point();
            newPoint.X = (int)((RotatePoint.X - Center.X) * Math.Cos(Angle) + (RotatePoint.Y - Center.Y) * Math.Sin(Angle) + Center.X);
            newPoint.Y = (int)(-(RotatePoint.X - Center.X) * Math.Sin(Angle) + (RotatePoint.Y - Center.Y) * Math.Cos(Angle) + Center.Y);
            return newPoint;
        }
        public static Mat RotateImg(Image<Bgr, byte> img,Point center,double angle)
        {
            Mat rotMatrix = new Mat();
            CvInvoke.GetRotationMatrix2D(center, angle, 1, rotMatrix);
            Mat imgRotated = new Mat();
            CvInvoke.WarpAffine(img, imgRotated, rotMatrix, new Size(img.Cols, img.Rows));
            return imgRotated;
        }
        public ResultLocation Locate(Image<Bgr, byte> img, IProperty property, Point samplePoint, double angle,Trigger trigger)
        {
            
            ResultLocation result = new ResultLocation();
            string js = JsonConvert.SerializeObject(property);
            _Properties = JsonConvert.DeserializeObject<Properties.Threshold>(js);
            CvInvoke.Rectangle(img, _Properties.ROI, new MCvScalar(0, 255, 0), 2);
            CvInvoke.Imwrite("img.jpg", img);
            img.ROI = _Properties.ROI;
            //ShowImgCapture(img);
            Mat imgThreshold = ReturnImgThreshold(img.Mat, _Properties.Thresh);

            CvInvoke.Imwrite("thresh.jpg", imgThreshold);
            VectorOfPoint maxCnt = FindlargestContour(imgThreshold);
            var shapeObject = CvInvoke.MinAreaRect(maxCnt);
            Point center = new Point((int)(shapeObject.Center.X+_Properties.ROI.X), (int)(shapeObject.Center.Y + _Properties.ROI.Y));
            //CvInvoke.Circle(imgThreshold, new Point((int)(shapeObject.Center.X),(int)(shapeObject.Center.Y)), 20, new MCvScalar(0, 255, 0), -1);
            //PointF[] list_point = getPoints(shapeObject);
            //System.Drawing.Point A = new System.Drawing.Point((int)(list_point[0].X), (int)(list_point[0].Y));
            //System.Drawing.Point B = new System.Drawing.Point((int)(list_point[1].X), (int)(list_point[1].Y));
            //System.Drawing.Point C = new System.Drawing.Point((int)(list_point[2].X), (int)(list_point[2].Y));
            //System.Drawing.Point D = new System.Drawing.Point((int)(list_point[3].X), (int)(list_point[3].Y));
            //System.Drawing.Point[] ABCD = new System.Drawing.Point[] { A, B, C, D };
            //CvInvoke.Polylines(imgThreshold, ABCD, true, new MCvScalar(0, 255, 9), 3);

            CvInvoke.Imwrite("thresh2.jpg", imgThreshold);

            double deviationAngle = getAngleLocate(shapeObject);
            if (trigger.IsLink)
            {
                double deltaAngle = deviationAngle - angle;
                
                double angleRadian = deltaAngle * Math.PI / 180;
                Point centerRotated = PointRotation(new Point((int)center.X, (int)center.Y), trigger.CenterRotation, angleRadian);
                img.ROI = new Rectangle();
                CvInvoke.Circle(img, centerRotated, 5, new MCvScalar(0, 255, 0), -1);
                var imgRotated = RotateImg(img, center, deltaAngle);
                CvInvoke.Imwrite("imgRotated.jpg", imgRotated);
                result.X_Offset = samplePoint.X - centerRotated.X;
                result.Y_Offset = samplePoint.Y - centerRotated.Y;
                result.R_Offet = -deltaAngle;
            }
            else
            {
                result.X_Offset = samplePoint.X - center.X;
                result.Y_Offset = samplePoint.Y - center.Y;
                result.R_Offet = angle - deviationAngle;
            }
            
            return result;
        }

        public List<bool> Inspect(Bitmap img, List<IProperty> property, string snPrint, ResultLocation offsetValue)
        {
            throw new NotImplementedException();
        }
    }
}
