﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Stick_Label_F16.Algorithm.Properties;
using Stick_Label_F16.Model;
using Stick_Label_F16.Config;
namespace Stick_Label_F16.Algorithm
{
    public interface IProcessing
    {
        ResultLocation Locate(Image<Bgr, byte> img, IProperty property,Point samplePoint, double angle,Trigger trigger);
        List<bool> Inspect(Bitmap img,List<IProperty> property,string snPrint, ResultLocation offsetValue);
    }
    public class ResultLocation
    {
        public Status Status { get; set; }
        public double X_Offset { get; set; }
        public double Y_Offset { get; set; }
        public double R_Offet { get; set; }
        public int Index { get; set; }
        public ResultLocation()
        {
            Status = Status.OK;
            X_Offset = 0;
            Y_Offset = 0;
            R_Offet = 0;
            Index = -1;
        }
    }

}
