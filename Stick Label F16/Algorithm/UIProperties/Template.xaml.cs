﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Stick_Label_F16.Algorithm.Locate;
using Stick_Label_F16.Algorithm.Properties;
using Newtonsoft.Json;

namespace Stick_Label_F16.Algorithm.UIProperties
{
    /// <summary>
    /// Interaction logic for Template.xaml
    /// </summary>
    public partial class Template : UserControl, INotifyPropertyChanged, IUI
    {
        private Views.ModelWindows.ProductWindows _ProductWindow = null;
        private Properties.Template _Properties = null;
        //private Properties.Settings _Param = Properties.Settings.Default;
        private System.Timers.Timer _TimerTemplate = new System.Timers.Timer(100);
        private Type TYPE = Type.GetType("Stick_Label_F16.Algorithm.Properties.Template");
        private bool _IsRuning = true;
        public Template(Views.ModelWindows.ProductWindows productWindow)
        {
            _ProductWindow = productWindow;
            load();
            InitializeComponent();
            this.DataContext = this;
            _TimerTemplate.Elapsed += OnTimedTemplate;
            _TimerTemplate.Enabled = true;
        }
        public Properties.Template mProperties
        {
            get
            {
                return _Properties;
            }
            set
            {
                _Properties = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        private void OnTimedTemplate(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerTemplate.Enabled = false;
            int tabIndex = -1;
            string name_algorithm = "";
            _ProductWindow.Dispatcher.Invoke(() =>
            {
                tabIndex = _ProductWindow.tabMain.SelectedIndex;
                name_algorithm = _ProductWindow.algorithms.Text;
            });
            if (name_algorithm != "Template")
                _IsRuning = false;
            
            if (!_ProductWindow.Draw && tabIndex == 0)
            {
                var bm = _ProductWindow.Camera.GetOneBitmap(1000);
                if (bm != null)
                {
                    using (Image<Bgr, byte> image = new Image<Bgr, byte>(bm))
                    {
                        //Image<Bgr, byte> image = new Image<Bgr, byte>(bm);
                        string savePath = $"{Const.ModelPath}/{_ProductWindow._Model.ModelType}/Properties/{_ProductWindow._IdLabel}/Locate";
                        if (!Directory.Exists(savePath))
                            Directory.CreateDirectory(savePath);
                        string savePathTemplate = $"{savePath}/{_ProductWindow._IdLabel}.jpg";
                        if (_ProductWindow.IsSetNewTemplate)
                        {
                            _Properties.ROI = _ProductWindow.RoiLocate;
                            _ProductWindow._ObjectLocation.Locate.MarkPoint = new System.Drawing.Point(_Properties.ROI.X, _Properties.ROI.Y);
                            image.ROI = _Properties.ROI;
                            Image<Bgr, byte> template = image.Copy();

                            if (File.Exists(savePathTemplate))
                            {
                                File.Delete(savePathTemplate);
                            }
                            CvInvoke.Imwrite(savePathTemplate, template);
                            _Properties.PathTemplate = savePathTemplate;
                            image.ROI = new System.Drawing.Rectangle();
                            _ProductWindow.IsSetNewTemplate = false;

                        }
                        if (_Properties.ROI != new System.Drawing.Rectangle())
                        {
                            if (new System.Drawing.Rectangle(0, 0, image.Width, image.Height).Contains(_Properties.ROI))
                            {
                                if (File.Exists(savePathTemplate) && _Properties.PathTemplate!=null)
                                {
                                    double smallest = 0.03;
                                    int inflateX = Convert.ToInt32(_Properties.SearchX / smallest);
                                    int inflateY = Convert.ToInt32(_Properties.SearchY / smallest);
                                    var roi = _Properties.ROI;
                                    var r = new System.Drawing.Rectangle(roi.X, roi.Y, roi.Width, roi.Height);
                                    r.Inflate(inflateX, inflateY);
                                    Mat img_template = CvInvoke.Imread(_Properties.PathTemplate);

                                    var result = Locate.Template.BestTemplateMatching(image, img_template.ToImage<Bgr, byte>(), r, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed);
                                    if (result.Score * 100 > _Properties.MatchingScore)
                                    {
                                        CvInvoke.Rectangle(image, result.Location, new MCvScalar(0, 255, 0), 2);
                                    }
                                    else
                                    {
                                        CvInvoke.Rectangle(image, result.Location, new MCvScalar(0, 0, 255), 2);
                                    }
                                    CvInvoke.Rectangle(image, r, new MCvScalar(255, 0, 0), 2);
                                    CvInvoke.PutText(image, "Search Template Area", new System.Drawing.Point(r.X, r.Y - 10), Emgu.CV.CvEnum.FontFace.HersheyComplexSmall, 1, new MCvScalar(255, 0, 0));
                                    this.Dispatcher.Invoke(() =>
                                    {
                                        lbRealScore_box.Content = $"Score: {Math.Round(result.Score * 100, 1)}%";

                                        imbTemplate_box.Source = _ProductWindow.imb.Bitmap2BitmapSource(img_template.Bitmap);

                                    });
                                }
                            }
                        }

                        _ProductWindow.Dispatcher.Invoke(() =>
                        {
                            _ProductWindow.imb.SourceFromBitmap = image.Bitmap;
                        });
                    }
                    //GC.Collect();
                    //GC.WaitForPendingFinalizers();
                }
            }
            
            _TimerTemplate.Enabled = _IsRuning;
        }
        private void load()
        {
            IProperty Intance = _ProductWindow._ObjectLocation.Locate.Property;
            if (Intance != null)
            {
                string js = JsonConvert.SerializeObject(Intance);
                _Properties = JsonConvert.DeserializeObject<Properties.Template>(js);
            }

        }
        public void InitPropertyUi()
        {
            _ProductWindow.properties_control.Children.Add(this);
        }
        private void btDraw_Click(object sender, RoutedEventArgs e)
        {
            _ProductWindow.Draw = true;
            _ProductWindow.imb.SetDrawRectMode();
        }

        private void txtMatchingScore_Box_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void cbMode_Positioning_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void txtSearchX_Box_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }

            _Properties.SearchX = value;
            OnPropertyChanged();
        }

        private void txtSearchY_Box_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }

            _Properties.SearchY = value;
            OnPropertyChanged();
        }
        private void Save()
        {
            if (TYPE != null)
            {
                IProperty instance = (IProperty)Activator.CreateInstance(TYPE);
                instance = _Properties;
                _ProductWindow._ObjectLocation.Locate.Property = instance;
                _ProductWindow._ObjectLocation.Locate.NameAlgorithmLocating = _ProductWindow.algorithms.SelectedItem.ToString();
                _ProductWindow._Model.Labels[_ProductWindow._IdLabel] = _ProductWindow._ObjectLocation;
                int res = _ProductWindow._Model.Save();
                if (res == 0)
                    MessageBox.Show("Save successfully !", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void btSave_Label_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }
    }
}
