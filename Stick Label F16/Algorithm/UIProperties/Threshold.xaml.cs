﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Stick_Label_F16.Model;
using Stick_Label_F16.Algorithm.Properties;
using Newtonsoft.Json;
using System.Reflection;
namespace Stick_Label_F16.Algorithm.UIProperties
{
    /// <summary>
    /// Interaction logic for UI.xaml
    /// </summary>
    public partial class Threshold : UserControl, INotifyPropertyChanged, IUI
    {
        private Views.ModelWindows.ProductWindows _ProductWindow = null;
        private Properties.Threshold _Properties = new Properties.Threshold();
        private System.Timers.Timer _TimerTheshold = new System.Timers.Timer(100);
        private Type TYPE = Type.GetType("Stick_Label_F16.Algorithm.Properties.Threshold");
        private bool _IsRuning = true;
        public Threshold(Views.ModelWindows.ProductWindows productWindow)
        {
            _ProductWindow = productWindow;
            load();
            InitializeComponent();
            this.DataContext = this;
            _TimerTheshold.Elapsed += OnTimerThreshold;
            _TimerTheshold.Enabled = true;
        }
        private void load()
        {
            IProperty Intance = _ProductWindow._ObjectLocation.Locate.Property;
            if (Intance != null)
            {
                string js = JsonConvert.SerializeObject(Intance);
                _Properties = JsonConvert.DeserializeObject<Properties.Threshold>(js);
            }
            
        }
        public void InitPropertyUi()
        {
            _ProductWindow.properties_control.Children.Add(this);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        public Properties.Threshold mProperties
        {
            get
            {
                return _Properties;
            }
            set
            {
                _Properties = value;
                OnPropertyChanged();
            }
        }
        private void OnTimerThreshold(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerTheshold.Enabled = false;
            int tabIndex = -1;
            string name_algorithm = "";
            _ProductWindow.Dispatcher.Invoke(() =>
            {
                tabIndex = _ProductWindow.tabMain.SelectedIndex;
                name_algorithm = _ProductWindow.algorithms.Text;
            });

            if (name_algorithm != "Threshold")
            {
                _IsRuning = false;
            }
            if (!_ProductWindow.Draw && tabIndex == 0)
            {
                var bm = _ProductWindow.Camera.GetOneBitmap(1000);
                if (bm != null)
                {
                    using (Image<Bgr, byte> image = new Image<Bgr, byte>(bm))
                    {
                        if (_ProductWindow.IsSetNewTemplate)
                        {
                            _Properties.ROI = _ProductWindow.RoiLocate;
                            CvInvoke.Imwrite($@"C:\StickLabelF16\ImageSaved\img_setUpThreshold{_ProductWindow._IdLabel+1}.jpg", image);
                            OnPropertyChanged();
                            _ProductWindow.IsSetNewTemplate = false;
                        }  
                        if (_Properties.ROI != new System.Drawing.Rectangle())
                        {

                            if (new System.Drawing.Rectangle(0, 0, image.Width, image.Height).Contains(_Properties.ROI))
                            {
                                image.ROI = _Properties.ROI;
                                int threshold = 0;
                                this.Dispatcher.Invoke(() => {
                                    threshold = Convert.ToInt32(thresh.Value);
                                });
                                //ReturnImgThresh(image.Mat, threshold);
                                
                                Mat ImgTheshold = Locate.Threshold.ReturnImgThreshold(image.Mat, threshold);
                                this.Dispatcher.Invoke(() =>
                                {
                                    img_threshold.Source = _ProductWindow.imb.Bitmap2BitmapSource(ImgTheshold.Bitmap);
                                });
                                VectorOfPoint cnt = Locate.Threshold.FindlargestContour(ImgTheshold);
                                if (cnt != null)
                                {
                                    if (_Properties.LabelType == 0)
                                    {
                                        var points = Locate.Threshold.getCenterContour(cnt);
                                        PointF O = points.Center;
                                        System.Drawing.Point point_O = new System.Drawing.Point((int)(O.X + _Properties.ROI.X), (int)(O.Y + _Properties.ROI.Y));
                                        PointF[] list_point = Locate.Threshold.getPoints(points);
                                        double angle_locate = Locate.Threshold.getAngleLocate(points);
                                        this.Dispatcher.Invoke(() =>
                                        {
                                            angle.Text = angle_locate.ToString();
                                        });
                                        _ProductWindow._ObjectLocation.Locate.Angle = angle_locate;  //goc lech cua label khi set up
                                        double angle_rotation_radian = angle_locate * (Math.PI / 180);
                                        //System.Drawing.Point center_rotated = Locate.Threshold.PointRotation(point_O, _ProductWindow.Triggers.ListTriggers[_ProductWindow._IdLabel].CenterRotation, angle_rotation_radian);
                                        _ProductWindow._ObjectLocation.Locate.MarkPoint = point_O;  //markPoint la tam cua label
                                        if (list_point.Count() > 0)
                                        {
                                            System.Drawing.Point A = new System.Drawing.Point((int)(list_point[0].X + _Properties.ROI.X), (int)(list_point[0].Y + _Properties.ROI.Y));
                                            System.Drawing.Point B = new System.Drawing.Point((int)(list_point[1].X + _Properties.ROI.X), (int)(list_point[1].Y + _Properties.ROI.Y));
                                            System.Drawing.Point C = new System.Drawing.Point((int)(list_point[2].X + _Properties.ROI.X), (int)(list_point[2].Y + _Properties.ROI.Y));
                                            System.Drawing.Point D = new System.Drawing.Point((int)(list_point[3].X + _Properties.ROI.X), (int)(list_point[3].Y + _Properties.ROI.Y));
                                            System.Drawing.Point[] ABCD = new System.Drawing.Point[] { A, B, C, D };
                                            image.ROI = new System.Drawing.Rectangle();
                                            CvInvoke.Polylines(image, ABCD, true, new MCvScalar(0, 255, 9), 3);
                                            CvInvoke.Circle(image, point_O, 10, new MCvScalar(255, 0, 0), -1);
                                            //CvInvoke.Circle(image, center_rotated, 10, new MCvScalar(100, 100, 100), -1);


                                        }

                                    }
                                    else
                                    {

                                        Mat img = image.Mat.Clone();
                                        Mat img_gray = new Mat();
                                        CvInvoke.CvtColor(img, img_gray, ColorConversion.Bgr2Gray);
                                        Mat img_smooth = new Mat();
                                        CvInvoke.GaussianBlur(img_gray, img_smooth, new System.Drawing.Size(7, 7), 0);
                                        Mat thresh = new Mat();
                                        CvInvoke.Threshold(img_smooth, thresh, threshold, 255, ThresholdType.Binary);
                                        int max_radius = 0;
                                        this.Dispatcher.Invoke(() => {
                                            try
                                            {
                                                max_radius = Convert.ToInt32(Convert.ToInt32(radius.Text) * 19);
                                            }
                                            catch { };
                                        });

                                        CircleF[] circles = CvInvoke.HoughCircles(thresh, HoughType.Gradient, 1, 100, 10, 10, max_radius - 10, max_radius);
                                        double r_max = 0;
                                        CircleF circle_max = new CircleF();
                                        foreach (CircleF circle in circles)
                                        {
                                            if (circle.Radius > r_max)
                                            {
                                                r_max = circle.Radius;
                                                circle_max = circle;
                                            }
                                        }
                                        System.Drawing.Point circle_fact = new System.Drawing.Point((int)(circle_max.Center.X + _Properties.ROI.X), (int)(circle_max.Center.Y + _Properties.ROI.Y));
                                        _ProductWindow._ObjectLocation.Locate.MarkPoint = circle_fact;
                                        _ProductWindow._ObjectLocation.Locate.Angle = 0; // hinh tron k can goc lech
                                        image.ROI = new System.Drawing.Rectangle();
                                        CvInvoke.Circle(image, circle_fact, (int)circle_max.Radius, new MCvScalar(0, 255, 0), 3);
                                        CvInvoke.Circle(image, circle_fact, 5, new MCvScalar(0, 255, 0), -3);
                                    }

                                }
                                else
                                    image.ROI = new System.Drawing.Rectangle();

                            }
                            CvInvoke.Rectangle(image, _Properties.ROI, new MCvScalar(0, 255, 255), 2);
                            CvInvoke.PutText(image, "Roi Area", new System.Drawing.Point(_Properties.ROI.X, _Properties.ROI.Y - 10), Emgu.CV.CvEnum.FontFace.HersheyComplexSmall, 1, new MCvScalar(0, 255, 255));
                        }
                        _ProductWindow.Dispatcher.Invoke(() =>
                        {
                            _ProductWindow.imb.SourceFromBitmap = image.Bitmap;
                        });
                    }

                }
            }
            _TimerTheshold.Enabled = _IsRuning;

        }
        private Mat ReturnImgThresh(Mat img, int thresh)
        {
            Mat img_gray = new Mat();
            CvInvoke.CvtColor(img, img_gray, ColorConversion.Bgr2Gray);
            Mat img_smooth = new Mat();
            CvInvoke.GaussianBlur(img_gray, img_smooth, new System.Drawing.Size(7, 7), 0);
            Mat thresh_img = new Mat();
            CvInvoke.Threshold(img_smooth, thresh_img, thresh, 255, ThresholdType.Binary);
            Mat img_resize = new Mat();
            CvInvoke.Resize(thresh_img, img_resize, new System.Drawing.Size(400, 250));
            
            return thresh_img;
        }
        private void cdType_label_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _Properties.LabelType = type_label.SelectedIndex;
        }

        private void btDraw_Click(object sender, RoutedEventArgs e)
        {
            _ProductWindow.Draw = true;
            _ProductWindow.imb.SetDrawRectMode();
        }
        
        private void Save()
        {
            if (TYPE != null)
            {
                IProperty instance = (IProperty)Activator.CreateInstance(TYPE);
                instance = _Properties;
                _ProductWindow._ObjectLocation.Locate.Property = instance;
                _ProductWindow._ObjectLocation.Locate.NameAlgorithmLocating = _ProductWindow.algorithms.SelectedItem.ToString();
                _ProductWindow._Model.Labels[_ProductWindow._IdLabel] = _ProductWindow._ObjectLocation;
                int res = _ProductWindow._Model.Save();
                if (res == 0)
                    MessageBox.Show("Save successfully !", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            
        }
        private void btSave_Label_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }
    }
}
