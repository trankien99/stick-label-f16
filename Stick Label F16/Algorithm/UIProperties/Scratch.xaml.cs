﻿using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using Stick_Label_F16.Algorithm.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stick_Label_F16.Algorithm.UIProperties
{
    /// <summary>
    /// Interaction logic for Scratch.xaml
    /// </summary>
    public partial class Scratch : UserControl, INotifyPropertyChanged,IUI
    {
        private Views.ModelWindows.ProductWindows _ProductWindow = null;
        private Properties.Scratch _Properties = new Properties.Scratch();
        private System.Timers.Timer _TimerScratch = new System.Timers.Timer(100);
        private int _Index = 0;
        private bool _IsReadCodeInspection = false;
        private bool _IsRuning = true;
        public Scratch(Views.ModelWindows.ProductWindows productWindow,int index)
        {
            _ProductWindow = productWindow;
            _Index = index;
            InitializeComponent();
            this.DataContext = this;
            load();
            _TimerScratch.Elapsed += OnTimerScratch;
            _TimerScratch.Enabled = true;
        }
        public Properties.Scratch mProperties
        {
            get
            {
                return _Properties;
            }
            set
            {
                _Properties = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        public void InitPropertyUi()
        {
            _ProductWindow.property_inpection.Children.Add(this);
        }
        private void load()
        {
            if(_Index<= _ProductWindow._ObjectLocation.Inspection.Property.Count-1)
            {
                IProperty Intance = _ProductWindow._ObjectLocation.Inspection.Property[_Index];
                if (Intance != null)
                {
                    string js = JsonConvert.SerializeObject(Intance);
                    if(js.Contains(_Properties.FullName))
                        _Properties = JsonConvert.DeserializeObject<Properties.Scratch>(js);
                }
            }
            
        }
        private void OnTimerScratch(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerScratch.Enabled = false;
            int tabIndex = -1;
            string name_algorithm = "";
            _ProductWindow.Dispatcher.Invoke(() =>
            {
                tabIndex = _ProductWindow.tabMain.SelectedIndex;
                name_algorithm = _ProductWindow.cb_algrithm_inspections.Text;
            });
            if (name_algorithm != "Scratch" || _Index != _ProductWindow.IndexInspectionSelected)
                _IsRuning = false;
            if ( tabIndex == 1)
            {
                if (!_ProductWindow.Draw)
                {
                    var bm = _ProductWindow.Camera.GetOneBitmap(1000);
                    if (bm != null)
                    {
                        using (Image<Bgr, byte> image = new Image<Bgr, byte>(bm))
                        {
                            if (_ProductWindow.IsSetNewTemplate)
                            {
                                _Properties.ROI = _ProductWindow.RoiInspection;
                                _ProductWindow.IsSetNewTemplate = false;
                            }   
                            if (_Properties.ROI != new System.Drawing.Rectangle())
                            {
                                
                                if (new System.Drawing.Rectangle(0, 0, image.Width, image.Height).Contains(_Properties.ROI))
                                {
                                    image.ROI = _Properties.ROI;
                                    CvInvoke.Imwrite("code.png", image);
                                    image.ROI = new System.Drawing.Rectangle();
                                    if (_IsReadCodeInspection)
                                    {
                                        var result = Service.ReadCode.Decode(new string[] { "code.png" }, false);
                                        File.Delete("code.png");
                                        string msg = $"Not found";
                                        if (result != null)
                                        {
                                            msg = $"{result.SN}";
                                        }
                                        this.Dispatcher.Invoke(() => {
                                            show_code.Text = msg;
                                        });
                                        _IsReadCodeInspection = false;
                                    }
                                }
                                CvInvoke.Rectangle(image, _Properties.ROI, new MCvScalar(0, 255, 255), 2);
                                CvInvoke.PutText(image, "Code Area", new System.Drawing.Point(_Properties.ROI.X, _Properties.ROI.Y - 10), Emgu.CV.CvEnum.FontFace.HersheyComplexSmall, 1, new MCvScalar(0, 255, 255));
                            }
                            else
                            {
                                _IsReadCodeInspection = false;
                            }
                            _ProductWindow.Dispatcher.Invoke(() =>
                            {
                                _ProductWindow.imb.SourceFromBitmap = image.Bitmap;
                            });
                        }
                        
                    }
                }
            }
            _TimerScratch.Enabled = _IsRuning;
        }
        private void txtROIX_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void txtROIW_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtROIH_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtSearchW_SN_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtSearchH_SN_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btReadCodeInpection_click(object sender, RoutedEventArgs e)
        {
            _IsReadCodeInspection = true;
        }

        private void btDrawInspection_Click(object sender, RoutedEventArgs e)
        {
            _ProductWindow.Draw = true;
            _ProductWindow.imb.SetDrawRectMode();
        }
        private void Save()
        {
            Type TYPE = Type.GetType(_Properties.FullName);
            if (TYPE != null)
            {
                IProperty instance = (IProperty)Activator.CreateInstance(TYPE);
                instance = _Properties.Copy();
                if (_Index <= _ProductWindow._ObjectLocation.Inspection.Property.Count-1)
                    _ProductWindow._ObjectLocation.Inspection.Property[_Index] = instance;
                else
                    _ProductWindow._ObjectLocation.Inspection.Property.Add(instance);
                _ProductWindow._ObjectLocation.Inspection.NameAlgorithm = _ProductWindow.algorithms.SelectedItem.ToString();
                _ProductWindow._Model.Labels[_ProductWindow._IdLabel] = _ProductWindow._ObjectLocation;
                int res = _ProductWindow._Model.Save();
                if (res == 0)
                    MessageBox.Show("Save successfully !", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
        private void btSaveInspectionSettings_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void txtROIY_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
