﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Newtonsoft.Json;
using Stick_Label_F16.Algorithm.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stick_Label_F16.Algorithm.UIProperties
{
    /// <summary>
    /// Interaction logic for Eccentric.xaml
    /// </summary>
    public partial class Eccentric : UserControl, INotifyPropertyChanged, IUI
    {
        private Views.ModelWindows.ProductWindows _ProductWindow = null;
        private Properties.Eccentric _Properties = new Properties.Eccentric();
        private System.Timers.Timer _TimerEccentric = new System.Timers.Timer(100);
        private int _Index = 0;
        private bool _IsRuning = true;
        private bool _IsReadCode = false;
        public Eccentric(Views.ModelWindows.ProductWindows productWindow,int index)
        {
            _ProductWindow = productWindow;
            _Index = index;
            InitializeComponent();
            this.DataContext = this;
            load();
            _TimerEccentric.Elapsed += OnTimerScratch;
            _TimerEccentric.Enabled = true;
        }
        public Properties.Eccentric mProperties
        {
            get
            {
                return _Properties;
            }
            set
            {
                _Properties = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        public void InitPropertyUi()
        {
            _ProductWindow.property_inpection.Children.Add(this);
        }
        private void OnTimerScratch(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerEccentric.Enabled = false;
            int tabIndex = -1;
            string name_algorithm = "";
            _ProductWindow.Dispatcher.Invoke(() =>
            {
                tabIndex = _ProductWindow.tabMain.SelectedIndex;
                name_algorithm = _ProductWindow.cb_algrithm_inspections.Text;
            });
            if (name_algorithm != "Eccentric" || _Index != _ProductWindow.IndexInspectionSelected)
                _IsRuning = false;
            if (tabIndex == 1)
            {
                if (!_ProductWindow.Draw)
                {
                    var bm = _ProductWindow.Camera.GetOneBitmap(1000);
                    if (bm != null)
                    {
                        using (Image<Bgr, byte> image = new Image<Bgr, byte>(bm))
                        {
                            if (_ProductWindow.IsSetNewTemplate)
                            {
                                _Properties.ROI = _ProductWindow.RoiInspection;
                                CvInvoke.Imwrite($@"C:\StickLabelF16\ImageSaved\InspectionLabel_{_Index + 1}.jpg", image);
                                _ProductWindow.IsSetNewTemplate = false;
                                OnPropertyChanged();
                            }
                            if (_Properties.ROI != new System.Drawing.Rectangle())
                            {

                                if (new System.Drawing.Rectangle(0, 0, image.Width, image.Height).Contains(_Properties.ROI))
                                {
                                    image.ROI = _Properties.ROI;
                                    int threshold = 0;
                                    int h_close = 1;
                                    ThresholdType thresholdType = ThresholdType.Binary;
                                    this.Dispatcher.Invoke(() =>
                                    {
                                        threshold = Convert.ToInt32(thresh.Value);
                                        h_close = Convert.ToInt32(close.Value);
                                        if (ck_isInvert.IsChecked == true)
                                        {
                                            _Properties.IsInvert = true;
                                            thresholdType = ThresholdType.BinaryInv;
                                        }
                                        else
                                        {
                                            _Properties.IsInvert = false;
                                            thresholdType = ThresholdType.Binary;
                                        }
                                    });
                                     Mat ImgThreshold = Locate.Threshold.ReturnImgThreshold(image.Mat, threshold, type: thresholdType);


                                    var imgClosed = Algorithm.Inspection.Eccentric.close(ImgThreshold, h_close);  // close image theo chieu ngang
                                    this.Dispatcher.Invoke(() =>
                                    {
                                        img_threshold.Source = _ProductWindow.imb.Bitmap2BitmapSource(imgClosed.Bitmap);
                                    });
                                    VectorOfPoint cnt = Locate.Threshold.FindlargestContour(imgClosed);
                                    if (cnt != null)
                                    {
                                        var points = Locate.Threshold.getCenterContour(cnt);
                                        if(points.Center!=null)
                                        {
                                            PointF O = points.Center;
                                            System.Drawing.Point point_O = new System.Drawing.Point((int)(O.X + _Properties.ROI.X), (int)(O.Y + _Properties.ROI.Y));
                                            PointF[] list_point = Locate.Threshold.getPoints(points);
                                            _Properties.SamplePoint = point_O;
                                            _Properties.Thresh = threshold;
                                            _Properties.ValueClose = h_close;
                                            if (list_point.Count() > 0)
                                            {
                                                System.Drawing.Point A = new System.Drawing.Point((int)(list_point[0].X + _Properties.ROI.X), (int)(list_point[0].Y + _Properties.ROI.Y));
                                                System.Drawing.Point B = new System.Drawing.Point((int)(list_point[1].X + _Properties.ROI.X), (int)(list_point[1].Y + _Properties.ROI.Y));
                                                System.Drawing.Point C = new System.Drawing.Point((int)(list_point[2].X + _Properties.ROI.X), (int)(list_point[2].Y + _Properties.ROI.Y));
                                                System.Drawing.Point D = new System.Drawing.Point((int)(list_point[3].X + _Properties.ROI.X), (int)(list_point[3].Y + _Properties.ROI.Y));
                                                System.Drawing.Point[] ABCD = new System.Drawing.Point[] { A, B, C, D };
                                                double angle_locate = Locate.Threshold.getAngleLocate(points);
                                                _Properties.Angle = angle_locate;
                                                this.Dispatcher.Invoke(() =>
                                                {
                                                    angle.Text = angle_locate.ToString();
                                                    
                                                });
                                                
                                                if (_IsReadCode)
                                                {
                                                    string sn = Algorithm.Inspection.Eccentric.Decode(image, angle_locate);
                                                    if (sn == null)
                                                        sn = "NOT FOUND";
                                                    this.Dispatcher.Invoke(() =>
                                                    {
                                                        SN.Text = sn;

                                                    });
                                                    _IsReadCode = false;
                                                }
                                                image.ROI = new System.Drawing.Rectangle();
                                                CvInvoke.Polylines(image, ABCD, true, new MCvScalar(0, 255, 9), 2);
                                                CvInvoke.Circle(image, point_O, 5, new MCvScalar(255, 0, 0), -1);
                                            }
                                        }
                                        
                                    }
                                    else
                                        image.ROI = new System.Drawing.Rectangle();
                                }
                                CvInvoke.Rectangle(image, _Properties.ROI, new MCvScalar(0, 255, 255), 2);
                                CvInvoke.PutText(image, "Roi Area", new System.Drawing.Point(_Properties.ROI.X, _Properties.ROI.Y - 10), Emgu.CV.CvEnum.FontFace.HersheyComplexSmall, 1, new MCvScalar(0, 255, 255));
                            }
                            _ProductWindow.Dispatcher.Invoke(() =>
                            {
                                _ProductWindow.imb.SourceFromBitmap = image.Bitmap;
                            });
                        }
                    }
                }
            }
            //else
            //    _TimerEccentric.Enabled = false;
            _TimerEccentric.Enabled = _IsRuning;
        }
        private Mat DetectObject(Mat img, int thresh)
        {
            Mat img_gray = new Mat();
            CvInvoke.CvtColor(img, img_gray, ColorConversion.Bgr2Gray);
            Mat img_smooth = new Mat();
            CvInvoke.GaussianBlur(img_gray, img_smooth, new System.Drawing.Size(5, 5), 0);
            Mat thresh_img = new Mat();
            CvInvoke.Threshold(img_smooth, thresh_img, thresh, 255, ThresholdType.BinaryInv);
            Mat img_resize = new Mat();
            CvInvoke.Resize(thresh_img, img_resize, new System.Drawing.Size(400, 250));
            this.Dispatcher.Invoke(() =>
            {
                img_threshold.Source = _ProductWindow.imb.Bitmap2BitmapSource(img_resize.Bitmap);
            });
            return thresh_img;
        }
        private void load()
        {
            if(_Index<= _ProductWindow._ObjectLocation.Inspection.Property.Count-1)
            {

                IProperty Intance = _ProductWindow._ObjectLocation.Inspection.Property[_Index];
                if (Intance != null)
                {
                    string js = JsonConvert.SerializeObject(Intance);
                    if(js.Contains(_Properties.FullName))
                        _Properties = JsonConvert.DeserializeObject<Properties.Eccentric>(js);
                }
            }
        }
        private void Save()
        {
            Type TYPE = Type.GetType(_Properties.FullName);
            if (TYPE != null)
            {
                IProperty instance = (IProperty)Activator.CreateInstance(TYPE);
                instance = _Properties.Copy();
                if (_Index <= _ProductWindow._ObjectLocation.Inspection.Property.Count-1)
                    _ProductWindow._ObjectLocation.Inspection.Property[_Index] = instance;
                else
                    _ProductWindow._ObjectLocation.Inspection.Property.Add(instance);
                _ProductWindow._ObjectLocation.Inspection.NameAlgorithm = _ProductWindow.cb_algrithm_inspections.SelectedItem.ToString();
                _ProductWindow._Model.Labels[_ProductWindow._IdLabel] = _ProductWindow._ObjectLocation;
                int res = _ProductWindow._Model.Save();
                if (res == 0)
                    MessageBox.Show("Save successfully !", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
        private void btSaveInspectionSettings_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void txtROIX_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtROIY_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtROIW_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtROIH_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btDrawInspection_Click(object sender, RoutedEventArgs e)
        {
            _ProductWindow.Draw = true;
            _ProductWindow.imb.SetDrawRectMode();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void readCode_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void readCode_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _IsReadCode = true;
        }
    }
}
