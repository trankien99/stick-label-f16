﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Algorithm.Properties
{
    public interface IProperty
    {
        IProperty Copy();
    }
}
