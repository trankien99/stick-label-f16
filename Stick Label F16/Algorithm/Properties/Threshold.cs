﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;

namespace Stick_Label_F16.Algorithm.Properties
{
    public class Threshold:IProperty
    {
        public string FullName { get; }
        public Rectangle ROI { get; set; }
        public int LabelType { get; set; }
        public int Thresh { get; set; }
        public bool IsInvert { get; set; }
        public double Radius { get; set; }
        public double Angle { get; set;}
        public Threshold()
        {
            this.Thresh = 127;
            this.Radius = 0;
            this.FullName = this.GetType().FullName;
            IsInvert = false;
        }
        public IProperty Copy()
        {
            Threshold threshold = new Threshold();
            threshold.ROI = this.ROI;
            threshold.Thresh = this.Thresh;
            threshold.Radius = this.Radius;
            threshold.LabelType = this.LabelType;
            threshold.IsInvert = this.IsInvert;
            return threshold;
        }
    }
}
