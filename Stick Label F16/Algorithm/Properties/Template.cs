﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;

namespace Stick_Label_F16.Algorithm.Properties
{
    public class Template:IProperty
    {
        public string FullName { get;}
        public Rectangle ROI { get; set; }
        public int SearchX { get; set; }
        public int SearchY { get; set; }
        public string PathTemplate { get; set; }
        public int MatchingScore { get; set; }
        public Template()
        {
            this.SearchX = 3;
            this.SearchY = 3;
            this.MatchingScore = 70;
            this.FullName = this.GetType().FullName;
        }
        public IProperty Copy()
        {
            Template template = new Template();
            template.ROI = this.ROI;
            template.SearchX = this.SearchX;
            template.SearchY = this.SearchY;
            template.MatchingScore = this.MatchingScore;
            template.PathTemplate = this.PathTemplate;
            return template;
        }
    }
}
