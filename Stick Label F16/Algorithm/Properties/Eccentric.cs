﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Algorithm.Properties
{
    public class Eccentric:IProperty
    {
        public string FullName { get; set; }
        public Rectangle ROI { get; set; }
        public int Thresh { get; set; }
        public bool IsInvert { get; set; }
        public Point SamplePoint { get; set; }
        public int ValueClose { get; set; }
        public double Angle { get; set; }
        public Eccentric()
        {
            this.FullName = this.GetType().FullName;
            this.ValueClose = 1;
            IsInvert = false;
        }
        public IProperty Copy()
        {
            Eccentric eccentric = new Eccentric();
            eccentric.ROI = this.ROI;
            eccentric.Thresh = this.Thresh;
            eccentric.SamplePoint = this.SamplePoint;
            eccentric.Angle = this.Angle;
            eccentric.ValueClose = this.ValueClose;
            eccentric.IsInvert = this.IsInvert;
            return eccentric;
        }
    }
}
