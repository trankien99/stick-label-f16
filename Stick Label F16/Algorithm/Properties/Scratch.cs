﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Algorithm.Properties
{
    public class Scratch:IProperty
    {
        public string FullName { get; set; }
        public Rectangle ROI { get; set; }
        public Scratch()
        {
            this.FullName = this.GetType().FullName;
        }
        public IProperty Copy()
        {
            Scratch scratch = new Scratch();
            scratch.ROI = this.ROI;
            return scratch;
        }
    }
}
