﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LabelManager2;
using System.Collections;

namespace Stick_Label_F16.Devices.Printer
{
    public class CodeSoft6
    {
        private static CodeSoft6 mObjs = null;
        private LabelManager2.Application mAppLab;
        private Document mDocLab;
        private string[] mVariables;
        private string[] mObjName;
        private string[] mVariableForObj;
        public static CodeSoft6 GetInstance()
        {
            if (mObjs == null)
            {
                mObjs = new CodeSoft6();
            }
            return mObjs;
        }
        
        public int LoadDesign(string namePrinter, string path)
        {
            try
            {
                Dispose();
                mAppLab = new LabelManager2.Application();
                var name = mAppLab.GetType();
                var doc = mAppLab.Documents.Open(path, true);
                mDocLab = mAppLab.ActiveDocument;
                mDocLab.Printer.SwitchTo(namePrinter);
            }
            catch
            {
                return -1;
            }

            return 1;
        }
        public void Dispose()
        {
            if (mDocLab != null)
            {
                mDocLab.Close();
                mDocLab = null;
            }
            if (mAppLab != null)
            {
                mAppLab.Documents.CloseAll();
                mAppLab.Quit();
                mAppLab = null;
            }
        }
        public int SetData(string name_variable, string value)
        {

            mDocLab.DocObjects.Barcodes.Item(name_variable).VariableObject.Value = value;
            var new_value = mDocLab.DocObjects.Barcodes.Item(name_variable).VariableObject.Value;
            return new_value == value ? 1 : -1;
        }
        public void Print(int qty_label)
        {
            mDocLab.PrintDocument(qty_label);
        }
    }
}
