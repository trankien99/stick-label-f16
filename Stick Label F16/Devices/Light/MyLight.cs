﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stick_Label_F16.SDK;

namespace Stick_Label_F16.Devices.Light
{
    public class MyLight : DKZ3_TCP
    {
        public static MyLight _DKZ = new MyLight();
        public static MyLight GetInstance()
        {
            if(_DKZ != null)
            {
                _DKZ = new MyLight();
            }
            return _DKZ;
        }
    }
}
