﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace Stick_Label_F16.Devices.Scanner
{
    public class Honeywell1900
    {
        public static SerialPort mSerial = new SerialPort();
        public static Honeywell1900 instance = null;
        public string mCmdOn = Convert.ToChar(0x16).ToString() + Convert.ToChar(0x54).ToString() + Convert.ToChar(0x0d).ToString();
        public string mCmdOff = Convert.ToChar(0x16).ToString() + Convert.ToChar(0x55).ToString() + Convert.ToChar(0x0d).ToString();
        public int Connect(string COM)
        {            
            if (mSerial.IsOpen)
            {
                mSerial.Close();
            }
            mSerial.PortName = COM;
            mSerial.ReadTimeout = 2000;
            mSerial.BaudRate = 115200;
            try
            {
                mSerial.Open();
            }
            catch (Exception er){
                Console.WriteLine(er.Message);
                return -1; }
            return 0;
        }
        public int Disconnect()
        {
            if (mSerial.IsOpen)
            {
                mSerial.Close();
            }
            return 0;
        }

        public string Scan()
        {
            //string content = string.Empty;
            //if(mSerial.IsOpen)
            //{
            //    try
            //    {
            //        mSerial.Write(mCmdOn);
            //        Thread.Sleep(200);
            //        int buffer = mSerial.ReadBufferSize;
            //        if(buffer > 0)
            //        {
            //            byte[] buff = new byte[buffer];
            //            mSerial.Read(buff, 0, buff.Length);
            //            content = ASCIIEncoding.UTF8.GetString(buff);
            //            content = content.Replace('\0', ' ').Trim();
            //        }
            //    }
            //    catch { }
            //}
            //return content;
            string content = string.Empty;
            if (mSerial.IsOpen)
            {
                try
                {

                    mSerial.Write(mCmdOn);
                    while (true)
                    {
                        int c = mSerial.ReadChar();
                        content += Convert.ToChar(c);
                    }
                }
                catch { }
                if (!string.IsNullOrEmpty(content))
                {
                    for (int i = 1; i < content.Length; i++)
                    {
                        string s = content;
                        string subcontent = content.Substring(0, i);
                        s = s.Replace(subcontent, "");
                        if (string.IsNullOrEmpty(s))
                        {
                            content = subcontent;
                            break;
                        }
                    }
                }
                content.Trim();
            }
            return content;

        }
        public string scanByHand()
        {
            string content = string.Empty;
            if (mSerial.IsOpen)
            {
                try
                {
                    //mSerial.Write(mCmdOn);
                    Thread.Sleep(200);
                    int buffer = mSerial.ReadBufferSize;
                    if (buffer > 0)
                    {
                        byte[] buff = new byte[buffer];
                        mSerial.Read(buff, 0, buff.Length);
                        content = ASCIIEncoding.UTF8.GetString(buff);
                        content = content.Replace('\0', ' ').Trim();
                    }
                }
                catch { }
            }
            return content;
        }
        public void DisableScan()
        {

            if (mSerial.IsOpen)
            {
                try
                {
                    mSerial.Write(mCmdOff);
                    Thread.Sleep(20);
                }
                catch { }
            }
        }
        public static Honeywell1900 GetInstance()
        {
            if(instance == null)
            {
                instance = new Honeywell1900();
            }
            return instance;
        }
    }
}
