﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stick_Label_F16.SDK;
using System.IO;


namespace Stick_Label_F16.Devices
{
    class Camera
    {
        private static List<HikCamera> _Cameras = new List<HikCamera>();
        private static string[] mListCameraName = null;
        public static string[] GetCameraNames()
        {
            mListCameraName = HikCamera.GetCameraNames();
            return mListCameraName;
        }
        public static HikCamera GetInstance(string Name)
        {
            HikCamera camera = null;
               mListCameraName = HikCamera.GetCameraNames();
            if(mListCameraName.Contains(Name))
            {
                for (int i = 0; i < _Cameras.Count; i++)
                {
                    if (_Cameras[i]?.CameraName == Name)
                    { 
                        camera = _Cameras[i];
                        break;
                    }
                }
                if(camera == null)
                {
                    camera = new HikCamera(Name);
                    _Cameras.Add(camera);
                }
            }
            return camera;
        }
        
    }
}
