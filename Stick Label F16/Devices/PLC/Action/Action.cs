﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Stick_Label_F16.SDK;
namespace Stick_Label_F16.Devices.PLC.Action
{
    public class PLC2
    {
        public SDK.SLMP _PLC2 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_2, Properties.Settings.Default.PORT_PLC_2);

        public string BIT_BITLOCATION_0 { get; } = "M314";
        public string BIT_BITLOCATION_1 { get; } = "M310";
        public string BIT_BITLOCATION_2 { get; } = "M311";
        public string BIT_BITLOCATION_3 { get; } = "M312";
        public string BIT_BITLOCATION_4_5_6 { get; } = "M313";
        public string BIT_BITLOCATION_7 { get; } = "M315";
        public string BIT_LOAD { get; } = "M317";
        public string BIT_UNLOAD { get; } = "M316";
        public int Go(string bitName)
        {
            _PLC2.SetDevice(bitName, 1);
            var res = _PLC2.GetDevice(bitName);
            if (res.Value == 1)
                return 1;
            else
                return -1;
        }
        public void Reset()
        {
            _PLC2.SetDevice(BIT_BITLOCATION_0, 0);
            _PLC2.SetDevice(BIT_BITLOCATION_1, 0);
            _PLC2.SetDevice(BIT_BITLOCATION_2, 0);
            _PLC2.SetDevice(BIT_BITLOCATION_3, 0);
            _PLC2.SetDevice(BIT_BITLOCATION_4_5_6, 0);
            _PLC2.SetDevice(BIT_BITLOCATION_7, 0);
            _PLC2.SetDevice(BIT_LOAD, 0);
        }
        private int SetReg(string SetReg, string CheckReg, int value)
        {
            bool isSetOK = false;
            for (int i = 0; i < 5; i++)
            {
                var result = _PLC2.SetDevice2(SetReg, value);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    SLMPResult resultGet = _PLC2.GetDevice2(CheckReg);
                    if (resultGet.Status == SLMPStatus.SUCCESSFULLY && resultGet.Value == value)
                    {
                        isSetOK = true;
                        break;
                    }
                }
            }
            return isSetOK ? 0 : -2;
        }
        private SLMPResult GetReg(string GetReg)
        {
            SLMPResult result = new SLMPResult();
            for (int i = 0; i < 5; i++)
            {
                result = _PLC2.GetDevice2(GetReg);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    break;
                }
            }
            return result;
        }
        public int SendCoorOffset(string regP,string regN,int value)
        {
            if (value > 0)
            {
                 int res = SetReg(regP, regP, value);
                return res == 0 ? 1 : -1;
            }
            else
            {
                int res = SetReg(regN, regN, -value);
                return res == 0 ? 1 : -1;
            }
        }
    }
    public class PLC1
    {
        public string BIT_BITLOCATION_0 { get; } = "M130";
        public SDK.SLMP _PLC1 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_1, Properties.Settings.Default.PORT_PLC_1);
        public int Go(string bitName)
        {
            _PLC1.SetDevice(bitName, 1);
            var res = _PLC1.GetDevice(bitName);
            if (res.Value == 1)
                return 1;
            else
                return -1;
        }
        private int SetReg(string SetReg, string CheckReg, int value)
        {
            bool isSetOK = false;
            for (int i = 0; i < 5; i++)
            {
                var result = _PLC1.SetDevice2(SetReg, value);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    SLMPResult resultGet = _PLC1.GetDevice2(CheckReg);
                    if (resultGet.Status == SLMPStatus.SUCCESSFULLY && resultGet.Value == value)
                    {
                        isSetOK = true;
                        break;
                    }
                }
            }
            return isSetOK ? 0 : -2;
        }
        private SLMPResult GetReg(string GetReg)
        {
            SLMPResult result = new SLMPResult();
            for (int i = 0; i < 5; i++)
            {
                result = _PLC1.GetDevice2(GetReg);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    break;
                }
            }
            return result;
        }
        public int SendCoorOffset(string regP, string regN, int value)
        {
            if (value > 0)
            {
                int res = SetReg(regP, regP, value);
                return res == 0 ? 1 : -1;
            }
            else
            {
                int res = SetReg(regN, regN, value);
                return res == 0 ? 1 : -1;
            }
        }
    }

}
