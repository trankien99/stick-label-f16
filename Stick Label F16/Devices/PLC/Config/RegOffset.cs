﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Devices.PLC.Config
{
    public class RegOffset
    {
        //cac thanh ghi bu cua label 1
        public string R_OFFSET_PICKUP_POS_1 { get; } = "D2408";
        public string R_OFFSET_PICKUP_NEG_1 { get; } = "D2410";
        public string X_OFFSET_PICKUP_POS_1 { get; } = "D2400";
        public string X_OFFSET_PICKUP_NEG_1 { get; } = "D2402";
        public string Y_OFFSET_PICKUP_NEG_1 { get; } = "D2406";
        public string Y_OFFSET_PICKUP_POS_1 { get; } = "D2404";

        public string R_OFFSET_STICK_POS_1 { get; } = "D2004";
        public string R_OFFSET_STICK_NEG_1 { get; } = "D2034";
        public string X_OFFSET_STICK_POS_1 { get; } = "D2000";
        public string X_OFFSET_STICK_NEG_1 { get; } = "D2030";
        public string Y_OFFSET_STICK_POS_1 { get; } = "D2002";
        public string Y_OFFSET_STICK_NEG_1 { get; } = "D2032";

        //cac thanh ghi bu cua label 2
        public string R_OFFSET_PICKUP_POS_2 { get; } = "D2448";
        public string R_OFFSET_PICKUP_NEG_2 { get; } = "D2450";
        public string X_OFFSET_PICKUP_POS_2 { get; } = "D2440";
        public string X_OFFSET_PICKUP_NEG_2 { get; } = "D2442";
        public string Y_OFFSET_PICKUP_NEG_2 { get; } = "D2446";
        public string Y_OFFSET_PICKUP_POS_2 { get; } = "D2444";

        public string R_OFFSET_STICK_POS_2 { get; } = "D2014";
        public string R_OFFSET_STICK_NEG_2 { get; } = "D2044";
        public string X_OFFSET_STICK_POS_2 { get; } = "D2010";
        public string X_OFFSET_STICK_NEG_2 { get; } = "D2040";
        public string Y_OFFSET_STICK_POS_2 { get; } = "D2012";
        public string Y_OFFSET_STICK_NEG_2 { get; } = "D2042";

        //cac thanh ghi bu cua label 3
        public string R_OFFSET_PICKUP_POS_3 { get; } = "D2488";
        public string R_OFFSET_PICKUP_NEG_3 { get; } = "D2490";
        public string X_OFFSET_PICKUP_POS_3 { get; } = "D2480";
        public string X_OFFSET_PICKUP_NEG_3 { get; } = "D2482";
        public string Y_OFFSET_PICKUP_NEG_3 { get; } = "D2486";
        public string Y_OFFSET_PICKUP_POS_3 { get; } = "D2484";

        public string R_OFFSET_STICK_POS_3 { get; } = "D2014";
        public string R_OFFSET_STICK_NEG_3 { get; } = "D2054";
        public string X_OFFSET_STICK_POS_3 { get; } = "D2010";
        public string X_OFFSET_STICK_NEG_3 { get; } = "D2050";
        public string Y_OFFSET_STICK_POS_3 { get; } = "D2012";
        public string Y_OFFSET_STICK_NEG_3 { get; } = "D2052";
    }
}
