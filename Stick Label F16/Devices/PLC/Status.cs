﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Stick_Label_F16.Devices.PLC
{
    public class Status
    {
        public SDK.SLMP _PLC1 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_1, Properties.Settings.Default.PORT_PLC_1);
        public SDK.SLMP _PLC2 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_2, Properties.Settings.Default.PORT_PLC_2);

        public string BIT_LOGIN { get; } = "S21";
        public string BIT_FASS { get; } = "M1";
        public string BIT_FAIL { get; } = "M2";
        public string BIT_FINISH { get; } = "M3";
        public string BIT_SELECT_TYPE_MODEL = "M5000";
        public string BIT_SET_LR { get; } = "M5001";
        public string BIT_SET_LT { get; } = "M5002";

        public string BIT_SCAN_B { get; } = "M1651"; //scan truoc 
        public string BIT_SCAN_A { get; } = "M1661";  // scan sau
        public string BIT_SCAN_FAIL_B { get; } = "M1653";
        public string BIT_SCAN_PASS_B { get; } = "M1652";
        public string BIT_SCAN_FAIL_A { get; } = "M1663";
        public string BIT_SCAN_PASS_A { get; } = "M1662";

        public string BIT_PRINT2_DONE { get; } = "M1622";
        public string BIT_PRINT3_DONE { get; } = "M1623";

        public string BIT_ON_LIGHT { get; } = "M1631";
        public int Login()
        {
            _PLC1.SetDevice(BIT_LOGIN, 1);
            _PLC2.SetDevice(BIT_LOGIN, 1);
            var res1 = _PLC1.GetDevice(BIT_LOGIN);
            var res2 = _PLC2.GetDevice(BIT_LOGIN);
            if (res1.Value == 1 && res2.Value == 1)
                return 1;
            else if (res1.Value != 1)
            {
                System.Windows.MessageBox.Show("Cant login to PLC 1", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            else
            {
                System.Windows.MessageBox.Show("Cant login to PLC 2", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -2;
            }
           
        }
        public int LogOut()
        {
            _PLC1.SetDevice(BIT_LOGIN, 0);
            _PLC2.SetDevice(BIT_LOGIN, 0);
            var res1 = _PLC1.GetDevice(BIT_LOGIN);
            var res2 = _PLC2.GetDevice(BIT_LOGIN);
            if (res1.Value == 0 && res2.Value == 0)
                return 1;
            else if (res1.Value != 0)
            {
                System.Windows.MessageBox.Show("Cant logout to PLC 1", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            else
            {
                System.Windows.MessageBox.Show("Cant logout to PLC 2", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -2;
            }
        }
        public void SelectModelType(int value1,int value2,int value3)
        {
            _PLC2.SetDevice(BIT_SELECT_TYPE_MODEL, value1);
            _PLC2.SetDevice(BIT_SET_LR, value2);
            _PLC2.SetDevice(BIT_SET_LT, value3);
        }
        public int WaiteFinish(int timeOut=5000)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int result = -1;
            while (sw.ElapsedMilliseconds < timeOut)
            {
                result = _PLC1.GetDevice(BIT_FINISH).Value;
                if (result == 1)
                    break; 
            }
            return result;
        }
    }
}
