﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Management;
using Renci.SshNet;
using Newtonsoft.Json.Linq;

namespace Stick_Label_F16.Devices.Fixture
{
    public class Fixture
    {
        
        //public string BIT_READ { get; } = "M";
        

        public List<string> LIST_LOCATION { get; } = new List<string>(5) {"M1521","M1522","M1523","M1524","M1525"};
        public List<string> ListFixturePass { get; } = new List<string>(5) {"M1601", "M1602", "M1603", "M1604", "M1605" };
        public List<string> ListFixtureFail { get; } = new List<string>(5) { "M1701", "M1702", "M1703", "M1704", "M1705" };
        public List<string> ListFixtureReady { get; } = new List<string>(5) { "M1531", "M1532", "M1533", "M1534", "M1535" };

        private string _IP = "192.168.1.20";
        private string PATH = @"F:\Tiep\TF16-11-1\MAC\Read-SN.txt";
        private List<int> _ListLocation = new List<int>();
        private List<string> _NameNetwork = new List<string>(5) { "fixture_1", "fixture_2", "fixture_3", "fixture_4", "fixture_5" };
        private SDK.SLMP _PLC1 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_1, Properties.Settings.Default.PORT_PLC_1);

        public int GetPing(int location)
        {
            Ping p = new Ping();
            PingReply r;
            
            try
            {
                r = p.Send(_IP);
            }
            catch
            {
                return -1;
            }
            return r.Status == IPStatus.Success ? 1 : -1;
        }
        public void EnableAdapter(string interfaceName)
        {
            string cmd = $"interface set interface {interfaceName} enable";
            ProcessStartInfo psi = new ProcessStartInfo("netsh", cmd);
            psi.Verb = "runas";
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            Process p = new Process();
            p.StartInfo = psi;
            p.Start();
        }
        public void DisableAdapter(string interfaceName)
        {
            string cmd = $"interface set interface {interfaceName} disable";
            ProcessStartInfo psi = new ProcessStartInfo("netsh", cmd);
            psi.Verb = "runas";
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            Process p = new Process();
            p.StartInfo = psi;
            p.Start();
        }

        public ResultReadSN ReadSN(int location,int timeOut=120000)
        {
            ResultReadSN result = new ResultReadSN();
            string code = "";
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < _NameNetwork.Count; i++)
            {
                if (i == location)
                {
                    Task one = Task.Factory.StartNew(() => EnableAdapter(_NameNetwork[i]));
                    one.Wait();
                }
                else
                {
                    Task one = Task.Factory.StartNew(() => DisableAdapter(_NameNetwork[i]));
                    one.Wait();
                }

            }
            while (sw.ElapsedMilliseconds < timeOut)
            {
                //int res = GetPing(location);
                //result.ResPing = 1;   //test
                int res = 1;
                if (res == 1)
                {
                    result.ResPing = 1;
                    //string s = getInforBySSH();
                    string s = "serialno=12345678\nqrid=abc";
                    if (s != null)
                    {
                        var listStr = s.Split('\n');
                        foreach (string str in listStr)
                        {
                            if (str.Contains("serialno"))
                            {
                                code += str.Split('=')[1].ToUpper();
                            }
                            else if (str.Contains("qrid"))
                            {
                                code += "-";
                                code += str.Split('=')[1];
                                break;
                            }
                        }
                        result.SN = code;
                        if (result.SN != "")
                        {
                            result.Status = true;
                            break;
                        }
                               
                    }
                    else
                    {
                        result.Status = false;
                    } 
                }
            }    
            return result;
        }
        public string getInforBySSH()
        {
            SshClient client = new SshClient("192.168.1.20", "ubnt", "ubnt");
            client.Connect();
            SshCommand cmd = client.CreateCommand("cat /proc/ubnthal/system.info");
            cmd.Execute();
            string ans = cmd.Result;
            return ans;
        }
        public int ReturnLocationOn(int timeOut = 60000)
        {
            int location = -1;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            while (sw.ElapsedMilliseconds < timeOut)
            {
                for(int i = 0; i < 5; i++)
                {
                    var res = _PLC1.GetDevice(LIST_LOCATION[i]);
                    if (res.Value == 1)
                    {
                        location = i;
                        _PLC1.SetDevice(LIST_LOCATION[i], 0);
                        break;
                    }
                        
                }
                if (location != -1)
                    break;
            }
            return location;
        }
        //public bool IsReady(int location,int timeOut=60000)
        //{
        //    bool isReady = false;
        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
        //    while (sw.ElapsedMilliseconds < timeOut)
        //    {
                
        //        var res = _PLC1.GetDevice(ListFixtureReady[location]);
        //        if (res.Value == 1)
        //        {
        //            _PLC1.SetDevice(ListFixtureReady[location], 0);
        //            isReady = true;
        //            break;
        //        }
                
        //    }
        //    return isReady;
        //}
    }
    public class ResultReadSN 
    {
        public string SN { get; set; } = "";
        public int ResPing { get; set; } = -1;
        public bool Status { get; set; } = false;
    }

}

