﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Stick_Label_F16.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "17.2.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:/StickLabelF16")]
        public string APP_CONFIG_PATH {
            get {
                return ((string)(this["APP_CONFIG_PATH"]));
            }
            set {
                this["APP_CONFIG_PATH"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Foxconn\\Projects\\StickLabelF16\\stick-label-f16\\Stick Label F16\\Algorithm\\Locat" +
            "e")]
        public string PATH_ALGORITHM_LOCATE {
            get {
                return ((string)(this["PATH_ALGORITHM_LOCATE"]));
            }
            set {
                this["PATH_ALGORITHM_LOCATE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("D:\\Foxconn\\Projects\\StickLabelF16\\stick-label-f16\\Stick Label F16\\Algorithm\\Inspe" +
            "ction")]
        public string PATH_ALGORITHM_INSPECTION {
            get {
                return ((string)(this["PATH_ALGORITHM_INSPECTION"]));
            }
            set {
                this["PATH_ALGORITHM_INSPECTION"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.03")]
        public double SMALLEST_FEATURE {
            get {
                return ((double)(this["SMALLEST_FEATURE"]));
            }
            set {
                this["SMALLEST_FEATURE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://127.0.0.1:812")]
        public string SERVICE_URL {
            get {
                return ((string)(this["SERVICE_URL"]));
            }
            set {
                this["SERVICE_URL"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.3.11")]
        public string IP_PLC_1 {
            get {
                return ((string)(this["IP_PLC_1"]));
            }
            set {
                this["IP_PLC_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1996")]
        public int PORT_PLC_1 {
            get {
                return ((int)(this["PORT_PLC_1"]));
            }
            set {
                this["PORT_PLC_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20")]
        public double CAM1_M_TO_P {
            get {
                return ((double)(this["CAM1_M_TO_P"]));
            }
            set {
                this["CAM1_M_TO_P"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0.5")]
        public double TOLERANCE {
            get {
                return ((double)(this["TOLERANCE"]));
            }
            set {
                this["TOLERANCE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0, 0")]
        public global::System.Drawing.Point CENTER_ROTATION_1 {
            get {
                return ((global::System.Drawing.Point)(this["CENTER_ROTATION_1"]));
            }
            set {
                this["CENTER_ROTATION_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.3.12")]
        public string IP_PLC_2 {
            get {
                return ((string)(this["IP_PLC_2"]));
            }
            set {
                this["IP_PLC_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1996")]
        public int PORT_PLC_2 {
            get {
                return ((int)(this["PORT_PLC_2"]));
            }
            set {
                this["PORT_PLC_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.3.4")]
        public string IP_LIGHT {
            get {
                return ((string)(this["IP_LIGHT"]));
            }
            set {
                this["IP_LIGHT"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.41")]
        public string IP_FIXTURE_1 {
            get {
                return ((string)(this["IP_FIXTURE_1"]));
            }
            set {
                this["IP_FIXTURE_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.42")]
        public string IP_FIXTURE_2 {
            get {
                return ((string)(this["IP_FIXTURE_2"]));
            }
            set {
                this["IP_FIXTURE_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.43")]
        public string IP_FIXTURE_3 {
            get {
                return ((string)(this["IP_FIXTURE_3"]));
            }
            set {
                this["IP_FIXTURE_3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.44")]
        public string IP_FIXTURE_4 {
            get {
                return ((string)(this["IP_FIXTURE_4"]));
            }
            set {
                this["IP_FIXTURE_4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.45")]
        public string IP_FIXTURE_5 {
            get {
                return ((string)(this["IP_FIXTURE_5"]));
            }
            set {
                this["IP_FIXTURE_5"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("COM4")]
        public string SCANNER_PORT {
            get {
                return ((string)(this["SCANNER_PORT"]));
            }
            set {
                this["SCANNER_PORT"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("55.5")]
        public double ANGLE_PULSE {
            get {
                return ((double)(this["ANGLE_PULSE"]));
            }
            set {
                this["ANGLE_PULSE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public double MM_PULSE {
            get {
                return ((double)(this["MM_PULSE"]));
            }
            set {
                this["MM_PULSE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool DIRECTION_X_1 {
            get {
                return ((bool)(this["DIRECTION_X_1"]));
            }
            set {
                this["DIRECTION_X_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool DIRECTION_Y_1 {
            get {
                return ((bool)(this["DIRECTION_Y_1"]));
            }
            set {
                this["DIRECTION_Y_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool DIRECTION_X_2 {
            get {
                return ((bool)(this["DIRECTION_X_2"]));
            }
            set {
                this["DIRECTION_X_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool DIRECTION_Y_2 {
            get {
                return ((bool)(this["DIRECTION_Y_2"]));
            }
            set {
                this["DIRECTION_Y_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("14.3")]
        public double PIXEL_MM_C1 {
            get {
                return ((double)(this["PIXEL_MM_C1"]));
            }
            set {
                this["PIXEL_MM_C1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("14.3")]
        public double PIXEL_MM_C2 {
            get {
                return ((double)(this["PIXEL_MM_C2"]));
            }
            set {
                this["PIXEL_MM_C2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("284, 909")]
        public global::System.Drawing.Point CENTER_1 {
            get {
                return ((global::System.Drawing.Point)(this["CENTER_1"]));
            }
            set {
                this["CENTER_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1315, 899")]
        public global::System.Drawing.Point CENTER_2 {
            get {
                return ((global::System.Drawing.Point)(this["CENTER_2"]));
            }
            set {
                this["CENTER_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("2361, 908")]
        public global::System.Drawing.Point CENTER_3 {
            get {
                return ((global::System.Drawing.Point)(this["CENTER_3"]));
            }
            set {
                this["CENTER_3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("14.3")]
        public double PIXEL_MM_C3 {
            get {
                return ((double)(this["PIXEL_MM_C3"]));
            }
            set {
                this["PIXEL_MM_C3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("5")]
        public double ANGLE_TOLERANCE {
            get {
                return ((double)(this["ANGLE_TOLERANCE"]));
            }
            set {
                this["ANGLE_TOLERANCE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("17.5")]
        public double PIXEL_MM_C2_2 {
            get {
                return ((double)(this["PIXEL_MM_C2_2"]));
            }
            set {
                this["PIXEL_MM_C2_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("UAP-AC-PRO")]
        public string NAME_PRODUCT1 {
            get {
                return ((string)(this["NAME_PRODUCT1"]));
            }
            set {
                this["NAME_PRODUCT1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("UAP-AC-LR")]
        public string NAME_PRODUCT2 {
            get {
                return ((string)(this["NAME_PRODUCT2"]));
            }
            set {
                this["NAME_PRODUCT2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("UAP-AC-LT")]
        public string NAME_PRODUCT3 {
            get {
                return ((string)(this["NAME_PRODUCT3"]));
            }
            set {
                this["NAME_PRODUCT3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.101")]
        public string IP_PRINTER_1 {
            get {
                return ((string)(this["IP_PRINTER_1"]));
            }
            set {
                this["IP_PRINTER_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("192.168.1.102")]
        public string IP_PRINTER_2 {
            get {
                return ((string)(this["IP_PRINTER_2"]));
            }
            set {
                this["IP_PRINTER_2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Users\\Administrator\\Desktop\\Document1.Lab")]
        public string PATH_DESIGN_CODE_2D {
            get {
                return ((string)(this["PATH_DESIGN_CODE_2D"]));
            }
            set {
                this["PATH_DESIGN_CODE_2D"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Users\\Administrator\\Desktop\\Document1.Lab")]
        public string PATH_DESIGN_CODE_1D {
            get {
                return ((string)(this["PATH_DESIGN_CODE_1D"]));
            }
            set {
                this["PATH_DESIGN_CODE_1D"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Stick_Label")]
        public string NAME {
            get {
                return ((string)(this["NAME"]));
            }
            set {
                this["NAME"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int ID_MACHINE {
            get {
                return ((int)(this["ID_MACHINE"]));
            }
            set {
                this["ID_MACHINE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("200.166.67.51")]
        public string IP {
            get {
                return ((string)(this["IP"]));
            }
            set {
                this["IP"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Stick_Label")]
        public string TYPE {
            get {
                return ((string)(this["TYPE"]));
            }
            set {
                this["TYPE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Packaging")]
        public string LINE {
            get {
                return ((string)(this["LINE"]));
            }
            set {
                this["LINE"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public int INDEX {
            get {
                return ((int)(this["INDEX"]));
            }
            set {
                this["INDEX"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("COM1")]
        public string LIGHT_PORT {
            get {
                return ((string)(this["LIGHT_PORT"]));
            }
            set {
                this["LIGHT_PORT"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("ZDesigner 110Xi4 600 dpi")]
        public string NAME_PRINTER_1 {
            get {
                return ((string)(this["NAME_PRINTER_1"]));
            }
            set {
                this["NAME_PRINTER_1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("ZDesigner 110Xi4 600 dpi (1)")]
        public string NAME_PRINTER_2 {
            get {
                return ((string)(this["NAME_PRINTER_2"]));
            }
            set {
                this["NAME_PRINTER_2"] = value;
            }
        }
    }
}
