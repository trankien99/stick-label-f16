﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stick_Label_F16.MyControl
{
    /// <summary>
    /// Interaction logic for Diagram.xaml
    /// </summary>
    public partial class Diagram : UserControl
    {
        public int Cols { get; set; }
        public int Rows { get; set; }
        private Config.Triggers TriggerConfigs = null; 
        public Diagram()
        {
            InitializeComponent();
        }
        public void Render(int Cols, int Rows)
        {
            this.Cols = Cols;
            this.Rows = Rows;
            grMain.Children.Clear();
            grMain.ColumnDefinitions.Clear();
            grMain.RowDefinitions.Clear();
            for (int i = 0; i < Cols; i++)
            {
                grMain.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < Rows; i++)
            {
                grMain.RowDefinitions.Add(new RowDefinition());
            }
            for (int x = 0; x < grMain.ColumnDefinitions.Count; x++)
            {
                for (int y = 0; y < grMain.RowDefinitions.Count; y++)
                {
                    Label label = new Label();
                    label.FontSize = 16;
                    label.FontWeight = FontWeights.Bold;
                    label.Content = $"Object {y * grMain.RowDefinitions.Count + x+1}";
                    label.HorizontalContentAlignment = HorizontalAlignment.Center;
                    label.VerticalContentAlignment = VerticalAlignment.Center;
                    Grid grid = new Grid();
                    Grid.SetColumn(grid, x);
                    Grid.SetRow(grid, y);
                    grid.Margin = new Thickness(2);
                    grid.Background = System.Windows.Media.Brushes.LightGray;
                    grid.MouseDown += PClick;
                    grid.Children.Add(label);
                    grMain.Children.Add(grid);
                }
            }
        }
        public void RenderMain(int Cols, int Rows)
        {
            this.Cols = Cols;
            this.Rows = Rows;
            grMain.Children.Clear();
            grMain.ColumnDefinitions.Clear();
            grMain.RowDefinitions.Clear();
            for (int i = 0; i < Cols; i++)
            {
                grMain.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < Rows; i++)
            {
                grMain.RowDefinitions.Add(new RowDefinition());
            }
            for (int x = 0; x < grMain.ColumnDefinitions.Count; x++)
            {
                for (int y = 0; y < grMain.RowDefinitions.Count; y++)
                {
                    TextBlock textBlock = new TextBlock();
                    textBlock.FontSize = 18;
                    textBlock.FontWeight = FontWeights.Bold;
                    textBlock.Text = "Watting ....";
                    textBlock.TextWrapping = TextWrapping.Wrap;
                    textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                    textBlock.VerticalAlignment = VerticalAlignment.Center;
                    Grid grid = new Grid();
                    Grid.SetColumn(grid, x);
                    Grid.SetRow(grid, y);
                    grid.Margin = new Thickness(2);
                    grid.Background = System.Windows.Media.Brushes.LightGray;
                    grid.Children.Add(textBlock);
                    grMain.Children.Add(grid);
                }
            }
        }
        public void RenderFixture(int cols,int rows)
        {
            this.Cols = cols;
            this.Rows = rows;
            grMain.Children.Clear();
            grMain.ColumnDefinitions.Clear();
            grMain.RowDefinitions.Clear();
            for (int i = 0; i < Cols; i++)
            {
                grMain.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < Rows; i++)
            {
                grMain.RowDefinitions.Add(new RowDefinition());
            }
            for (int x = 0; x < grMain.ColumnDefinitions.Count; x++)
            {
                for (int y = 0; y < grMain.RowDefinitions.Count; y++)
                {
                    StackPanel stackPanelMain = new StackPanel();
                    stackPanelMain.Orientation = Orientation.Vertical;

                    StackPanel passFail = new StackPanel();
                    passFail.Orientation = Orientation.Horizontal;
                    Label lbPass = new Label();
                    lbPass.Content = "Pass";
                    lbPass.Width = 100;
                    lbPass.HorizontalContentAlignment = HorizontalAlignment.Center;
                    
                    Label lbFail = new Label();
                    lbFail.Content = "Fail";
                    lbFail.Width = 100;
                    lbFail.HorizontalContentAlignment = HorizontalAlignment.Center;
                    passFail.Children.Add(lbPass);
                    passFail.Children.Add(lbFail);

                    StackPanel passFailResult = new StackPanel();
                    passFailResult.Orientation = Orientation.Horizontal;
                    passFailResult.Margin = new Thickness(2);

                    StackPanel Sn = new StackPanel();

                    Label lbPassResult = new Label();
                    lbPassResult.Name = "qtyPass";
                    lbPassResult.Width = 100;
                    lbPassResult.Height = 25;
                    lbPassResult.Background = Brushes.White;
                    lbPassResult.HorizontalContentAlignment = HorizontalAlignment.Center;
                    lbPassResult.Margin = new Thickness(1);

                    Label lbFailResult = new Label();
                    lbFailResult.Name = "qtyFail";
                    lbFailResult.Width = 100;
                    lbFailResult.Height = 25;
                    lbFailResult.Background = Brushes.White;
                    lbFailResult.HorizontalContentAlignment = HorizontalAlignment.Center;
                    lbFailResult.Margin = new Thickness(1);


                    passFailResult.Children.Add(lbPassResult);
                    passFailResult.Children.Add(lbFailResult);

                    Label lbSN = new Label();
                    lbSN.Name = "SN";
                    lbSN.Width = 200;
                    lbSN.Height = 50;
                    lbSN.Background = Brushes.White;
                    lbSN.HorizontalContentAlignment = HorizontalAlignment.Center;
                    lbSN.Margin = new Thickness(2);
                    Sn.Children.Add(lbSN);

                    stackPanelMain.Children.Add(passFail);
                    stackPanelMain.Children.Add(passFailResult);
                    stackPanelMain.Children.Add(Sn);

                    Border border = new Border();
                    border.BorderThickness = new Thickness(2);
                    border.BorderBrush = Brushes.LightGray;

                    Grid grid = new Grid();
                    Grid.SetColumn(grid, x);
                    Grid.SetRow(grid, y);
                    grid.Margin = new Thickness(1);
                    
                    grid.Background = System.Windows.Media.Brushes.LightGray;
                    grid.Children.Add(stackPanelMain);
                    grid.Children.Add(border);
                    grMain.Children.Add(grid);

                }
            }
        }
        public void RenderTriggerConfig(int Cols, Config.Triggers TriggerConfigs)
        {
            this.Cols = Cols;
            this.Rows = TriggerConfigs.QtyTrigger;
            grMain.Children.Clear();
            grMain.ColumnDefinitions.Clear();
            grMain.RowDefinitions.Clear();
            for (int i = 0; i < Cols; i++)
            {
                grMain.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < Rows; i++)
            {
                grMain.RowDefinitions.Add(new RowDefinition());
            }
            for (int x = 0; x < grMain.ColumnDefinitions.Count; x++)
            {
                for (int y = 0; y < grMain.RowDefinitions.Count; y++)
                {
                    StackPanel stackPanel = new StackPanel();
                    stackPanel.Orientation = Orientation.Vertical;

                    StackPanel subStackPanel = new StackPanel();
                    subStackPanel.Orientation = Orientation.Horizontal;

                    Label lb1 = new Label();
                    lb1.Width = 50;
                    lb1.FontSize = 12;
                    lb1.Content = Convert.ToString(y + 1);

                    Label lb2 = new Label();
                    lb2.Width = 100;
                    lb2.FontSize = 12;
                    lb2.Content = TriggerConfigs.ListTriggers[y].BitName;

                    Label lb3 = new Label();
                    lb3.Width = 100;
                    lb3.FontSize = 12;
                    lb3.Content = TriggerConfigs.ListTriggers[y].ObjectTarget.ToString();

                    Label lb4 = new Label();
                    lb4.Width = 100;
                    lb4.FontSize = 12;
                    lb4.Content = TriggerConfigs.ListTriggers[y].Type.ToString();

                    Label lb5 = new Label();
                    lb5.Width = 100;
                    lb5.FontSize = 12;
                    lb5.Content = TriggerConfigs.ListTriggers[y].Action;

                    Label lb6 = new Label();
                    lb6.Width = 100;
                    lb6.FontSize = 12;
                    lb6.Content = TriggerConfigs.ListTriggers[y].NameCamera;

                    Label lb7 = new Label();
                    lb7.Width = 100;
                    lb7.FontSize = 12;
                    lb7.Content = TriggerConfigs.ListTriggers[y].CenterRotation.ToString();

                    Label lb8 = new Label();
                    lb8.Width = 100;
                    lb8.FontSize = 12;
                    lb8.Content = TriggerConfigs.ListTriggers[y].Channel.ToString();

                    Label lb9 = new Label();
                    lb9.Width = 100;
                    lb9.FontSize = 12;
                    lb9.Content = TriggerConfigs.ListTriggers[y].BitPass;

                    Label lb10 = new Label();
                    lb10.Width = 100;
                    lb10.FontSize = 12;
                    lb10.Content = TriggerConfigs.ListTriggers[y].BitFail;

                    CheckBox ck = new CheckBox();
                    ck.VerticalAlignment = VerticalAlignment.Center;
                    ck.IsChecked = TriggerConfigs.ListTriggers[y].IsLink;

                    subStackPanel.Children.Add(lb1);
                    subStackPanel.Children.Add(lb2);
                    subStackPanel.Children.Add(lb3);
                    subStackPanel.Children.Add(lb4);
                    subStackPanel.Children.Add(lb5);
                    subStackPanel.Children.Add(lb6);
                    subStackPanel.Children.Add(lb7);
                    subStackPanel.Children.Add(lb8);
                    subStackPanel.Children.Add(lb9);
                    subStackPanel.Children.Add(lb10);
                    subStackPanel.Children.Add(ck);


                    stackPanel.Children.Add(subStackPanel);

                    Grid grid = new Grid();
                    Grid.SetColumn(grid, x);
                    Grid.SetRow(grid, y);
                    //grid.Margin = new Thickness(2);
                    grid.Background = System.Windows.Media.Brushes.LightGray;
                    grid.MouseDown += PClick;
                    grid.Children.Add(stackPanel);
                    grMain.Children.Add(grid);

                }
            }
        }
        public void SetColor(int Cols, int Rows, System.Windows.Media.Brush color)
        {
            foreach (var item in grMain.Children)
            {
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if(Cols == col && row == Rows)
                {
                    (item as Grid).Background = color;
                }
            }
        }
        public void SetFontSize(int fontSize)
        {
            foreach (var item in grMain.Children)
            {
                Label label = (item as Grid).Children[0] as Label;
                label.FontSize = fontSize;
            }
        }
        public void SetLabel(int Cols, int Rows, string content)
        {
            foreach (var item in grMain.Children)
            {
                int i = grMain.Children.Count;
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (Cols == col && row == Rows)
                {
                    Label label = (item as Grid).Children[0] as Label;
                    label.Content = content;
                }
            }
        }
        public void SetTextBlock(int cols, int rows, string content)
        {
            foreach (var item in grMain.Children)
            {
                int i = grMain.Children.Count;
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (cols == col && row == rows)
                {
                    TextBlock textBlock = (item as Grid).Children[0] as TextBlock;
                    textBlock.Text = content;
                }
            }
        }
        public void SetLabelFixture(int rows,string nameLabel,string content, int cols = 0)
        {
            foreach (var item in grMain.Children)
            {
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (cols == col && row == rows)
                {
                    StackPanel stackPanelMain = (item as Grid).Children[0] as StackPanel;
                    for (int j = 1; j < stackPanelMain.Children.Count; j++)
                    {
                        StackPanel stackPanel = stackPanelMain.Children[j] as StackPanel;
                        foreach (var child in stackPanel.Children)
                        {
                            Label label = child as Label;
                            if (label.Name == nameLabel)
                                label.Content = content;
                        }
                    }
                }
            }
        }
        public void SetBorderBrush(int rows,Brush color,int cols=0)
        {
            foreach (var item in grMain.Children)
            {
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (cols == col && row == rows)
                {
                    Border border = (item as Grid).Children[1] as Border;
                    border.BorderBrush = color;
                }
            }
        }
        public string GetLabel(int Cols, int Rows)
        {
            string content = string.Empty;
            foreach (var item in grMain.Children)
            {
                int col = Grid.GetColumn(item as UIElement);
                int row = Grid.GetRow(item as UIElement);
                if (Cols == col && row == Rows)
                {
                    Label label = (item as Grid).Children[0] as Label;
                    content = label.Content as string;
                }
            }
            return content;
        }
        public event MouseButtonEventHandler PClick;
        protected void PClick_Click(object sender, MouseButtonEventArgs e)
        {
            //bubble the event up to the parent
            if (this.PClick != null)
                this.PClick_Click(this, e);
        }

        internal void SetColor(int i, int v,System.Drawing.Brush green)
        {
            throw new NotImplementedException();
        }

        internal void SetColor(int i, int v, object green)
        {
            throw new NotImplementedException();
        }
    }
}
