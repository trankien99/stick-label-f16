﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16
{

    public class Const
    {
        public static string ModelPath = $"{Properties.Settings.Default.APP_CONFIG_PATH}/Models";
        public static string PLCPath = $"{Properties.Settings.Default.APP_CONFIG_PATH}/PLC";
        public static string TriggerConfigPath = $"{Properties.Settings.Default.APP_CONFIG_PATH}/TriggerConfig";
    }

}
