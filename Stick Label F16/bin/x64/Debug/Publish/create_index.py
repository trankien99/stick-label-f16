import pymongo

client = pymongo.MongoClient("mongodb://127.0.0.1", 27017)

db = client["SPIMachine"]
panel_col = db["PanelResult"]
pad_col = db["PadResult"]



panel_col.create_index("Sync")
panel_col.create_index("LoadTime")

pad_col.create_index("Time")
