import numpy as np
import cv2
import os
from flask import Flask, flash, request, redirect, url_for
import time
import datetime
import base64
from pyzbar.pyzbar import decode, bounding_box
import Decoder
import os

app = Flask(__name__)
# app.config["DEBUG"] = True

@app.route('/', methods=['GET', 'POST'])
def Action():
    global SEGMENT_MODEL
    if request.method == 'POST':
        result  = {}
        if(request.values.get("Type") == "Test"):
            result["status"] = "OK"
            return str(result)
        if 'file' not in request.files:
            flash('No file part')
            result["status"] = "FAIL"
            print("[INFO] | {} |{}".format(datetime.datetime.now(), "No file part"))
            return str(result)
        file = request.files.getlist("file")
        for f in file:
            if f.filename == '':
                result["status"] = "FAIL"
                print("[INFO] | {} |{}".format(datetime.datetime.now(), "Not found file"))
                return str(result)
            filestr = f.read()
            npimg = np.fromstring(filestr, np.uint8)
            action_type = request.values.get("Type")
            img = cv2.imdecode(npimg, 1)

            if action_type == "Decode":
                img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                sn = Decoder.Decode(img_gray)
                result["status"] = "OK"
                result["sn"] = sn
                print(str(result))
                return str(result)

    elif request.method == 'GET':
        s = os.path.dirname(os.path.abspath(__file__))
        return "Service laser marking : \n" + s
    

    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=812)
