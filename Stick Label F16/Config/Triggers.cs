﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Config
{
    public class Triggers
    {
        public int QtyTrigger { get; set; }
        public List<Trigger> ListTriggers {get;set;}
       
        public Triggers()
        {
            this.QtyTrigger = 1;
            this.ListTriggers = new List<Trigger>();
            
        }
        public int Save()
        {
            string savePath = $"{Const.TriggerConfigPath}";
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            string configPath = $"{savePath}/TriggerConfig.json";
            string js = JsonConvert.SerializeObject(this);
            File.WriteAllText(configPath, js);
            return 0;
        }
        public static Triggers LoadModel()
        {
            string path = $"{Const.TriggerConfigPath}/TriggerConfig.json";
            if (File.Exists(path))
            {
                string s = File.ReadAllText(path);
                Triggers triggers = null;
                try
                {
                    triggers = JsonConvert.DeserializeObject<Triggers>(s);
                }
                catch (Exception ex)
                {
                    return null;
                }
                if (triggers != null)
                {

                    return triggers;
                }
                else
                    return null;
            }
            else
                return null;
        }
    }
    public class Trigger
    {
        public string BitName { get; set; }
        public string BitPass { get; set; }
        public string BitFail { get; set; }
        public int ObjectTarget { get; set; }
        public int Type { get; set; }  // have 2 type :0:Object 1 and 1:object 2
        public string Action { get; set; }
        public string NameCamera { get; set; }
        public System.Drawing.Point CenterRotation { get; set; }
        public int Channel { get; set; }
        public bool IsLink { get; set; }
        public Trigger()
        {
            this.IsLink = true;
        }
        public Trigger Copy()
        {
            Trigger trigger = new Trigger();
            trigger.BitName = this.BitName;
            trigger.ObjectTarget = this.ObjectTarget;
            trigger.Type = this.Type;
            trigger.IsLink = this.IsLink;
            trigger.NameCamera = this.NameCamera;
            trigger.Channel = this.Channel;
            trigger.Action = this.Action;
            trigger.CenterRotation = this.CenterRotation;
            trigger.BitFail = this.BitFail;
            trigger.BitPass = this.BitPass;
            return trigger;
        }
    }
}
