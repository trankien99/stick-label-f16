﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Stick_Label_F16.Views.UserWindows;
using Stick_Label_F16.Views.ModelWindows;
using Stick_Label_F16.Algorithm;
using System.Threading;
using Emgu.CV;
using Emgu.CV.Structure;
using Renci.SshNet;
using System.Net.NetworkInformation;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Stick_Label_F16
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool IsRunning = false;
        private System.Timers.Timer _TimerMain = new System.Timers.Timer(100);
        private System.Timers.Timer _TimerReadSN = new System.Timers.Timer(100);
        private System.Timers.Timer _TimerScanA = new System.Timers.Timer(100);
        private Properties.Settings _Param = Properties.Settings.Default;
        public SDK.SLMP PLC2 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_2, Properties.Settings.Default.PORT_PLC_2);
        public SDK.SLMP PLC1 = new SDK.SLMP(Properties.Settings.Default.IP_PLC_1, Properties.Settings.Default.PORT_PLC_1);
        public Config.Triggers TriggerConfig = null;
        public Model.Model model = null;
        private Devices.Fixture.Fixture _Fixture = new Devices.Fixture.Fixture();
        private Devices.Printer.CodeSoft6 _Cs6 = Devices.Printer.CodeSoft6.GetInstance();
        public Devices.PLC.Status _Status = new Devices.PLC.Status();
        private Devices.Scanner.Honeywell1900 _Scanner = Devices.Scanner.Honeywell1900.GetInstance();
        private List<Link> _ListLink = new List<Link>();  //thong tin ve cac carier da doc
        private List<int> _ListFixtureON = new List<int>();

        public List<ResultLocation> ListResultLocate = new List<ResultLocation>(3);
        public List<List<ResultLocation>> ListTimeLocate = new List<List<ResultLocation>>();

        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr O);
        public MainWindow()
        {
            InitializeComponent();
            RefreshModels();
            diagramFixture.RenderFixture(1, 5);
            ListResultLocate = new List<ResultLocation>(3) { new ResultLocation(), new ResultLocation(), new ResultLocation()};
            //Database.Config config = new Database.Config();
            //Database.PingTime pingTime = new Database.PingTime();
            //Database.RawData rawData = new Database.RawData("PRO");
            //ConvertToJson(rawData, "Raw_data");
            //Print("aaaa");
        }
        public static BitmapSource ToBitmapSource(Image<Bgr, byte> image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap();
                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr, IntPtr.Zero, Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                DeleteObject(ptr);
                return bs;
            }
        }
        private void OnTimerScan(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerMain.Enabled = false;
            ScanBefore();
            _TimerMain.Enabled = IsRunning;
        }
        private void OnTimerReadSN(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerReadSN.Enabled = false;
            showImforFixture();
            if (_ListFixtureON.Count > 0)
            {
                this.Dispatcher.Invoke(() =>
                {
                    diagramFixture.SetBorderBrush(_ListFixtureON[0], Brushes.Yellow);
                });
                var res = PLC1.GetDevice(_Fixture.ListFixtureReady[_ListFixtureON[0]]);
                if (res.Value==1)
                {
                    var resReset = PLC1.SetDevice(_Fixture.ListFixtureReady[_ListFixtureON[0]],0);
                    ShowImfor("Pinging... 192.168.1.20", Brushes.Yellow);
                    Devices.Fixture.ResultReadSN code = _Fixture.ReadSN(_ListFixtureON[0]);
                    if (code.ResPing == 1)
                    {
                        if (code.Status)
                        {
                            for (int i = 0; i < _ListLink.Count; i++)
                            {
                                if (_ListLink[i].FixtureLocation == _ListFixtureON[0])
                                    _ListLink[i].SN = code.SN;

                            }
                            PLC1.SetDevice(_Fixture.ListFixturePass[_ListFixtureON[0]], 1);
                            _ListFixtureON.RemoveAt(0);
                        }
                        else
                        {
                            ShowImfor($"Fixture {_ListFixtureON[0]} không thể đọc dữ liệu từ sản phẩm \nFixture {_ListFixtureON[0]} can't read data from product", Brushes.Red);
                            PLC1.SetDevice(_Fixture.ListFixtureFail[_ListFixtureON[0]], 1);
                            _ListFixtureON.RemoveAt(0);
                        }

                    }
                    else
                    {
                        ShowImfor($"Fixture {_ListFixtureON[0]} không thể ping đến địa chỉ 192.168.1.20\nFixture {_ListFixtureON[0]} can't ping to 192.168.1.20", Brushes.Red);
                        PLC1.SetDevice(_Fixture.ListFixtureFail[_ListFixtureON[0]], 1);
                        _ListFixtureON.RemoveAt(0);
                    }
                }
   
            }
            
            _TimerReadSN.Enabled = IsRunning;
        }
        
        private void OnTimerScanA(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerScanA.Enabled = false;
            ScanAfter();
            _TimerScanA.Enabled = IsRunning;
        }
        private void ScanBefore()
        {
            var triggerScanBefore = PLC1.GetDevice(_Status.BIT_SCAN_B);
            if (triggerScanBefore.Value == 1)
            {
                PLC1.SetDevice(_Status.BIT_SCAN_B, 0);
                ShowImfor("Scanning ...", Brushes.Yellow);
                string carrierCode = _Scanner.Scan();
                if (carrierCode != "")
                {
                    ShowImfor(carrierCode, Brushes.Yellow);
                    PLC1.SetDevice(_Status.BIT_SCAN_PASS_B, 1);
                    Link link = new Link();
                    link.ScarierCode = carrierCode;
                    int location = _Fixture.ReturnLocationOn();
                    if (location != -1)
                    {
                        _ListFixtureON.Add(location);
                        link.FixtureLocation = location;
                        _ListLink.Add(link);
                    }
                    else
                    {
                        ShowImfor("Không tìm thấy vị trí fixture", Brushes.Red);
                    }

                }
                else
                {
                    ShowImfor("Không đọc được mã khuôn\nCant read code of carier", Brushes.Red);
                    PLC1.SetDevice(_Status.BIT_SCAN_FAIL_B, 1);
                }
                    
            }
        }
        
        private void ScanAfter()
        {
           
            var res = PLC1.GetDevice(_Status.BIT_SCAN_A);
            if (res.Value == 1)
            {
                ShowImfor("Scan again ...", Brushes.Yellow);
                PLC1.SetDevice(_Status.BIT_SCAN_A, 0);
               
                string scarierCode = _Scanner.Scan();
                if (scarierCode != "")
                {
                    PLC1.SetDevice(_Status.BIT_SCAN_PASS_A, 1);
                    for (int i = 0; i < _ListLink.Count; i++)
                    {
                        if(scarierCode == _ListLink[i].ScarierCode)
                        {
                            ShowImfor($"{scarierCode} ==> {_ListLink[i].SN}({_ListLink[i].FixtureLocation+1})", Brushes.Yellow);
                            Print(_ListLink[i].SN);

                            // phan loai modle gui PLC
                            //string modelType = getModelType();
                            //SetModelType(modelType);

                            PLC2.SetDevice(_Status.BIT_PRINT2_DONE, 1);
                            PLC2.SetDevice(_Status.BIT_PRINT3_DONE, 1);
                            _ListLink.RemoveAt(i);
                        }
                    }
                }
                else
                {
                    ShowImfor($"{scarierCode} không khớp với các carier đã đọc", Brushes.Red);
                    PLC1.SetDevice(_Status.BIT_SCAN_FAIL_A, 1);
                }
            }
        }
        private void showImforFixture()
        {
            for (int i = 0; i < 5; i++)
            {
                if (_ListFixtureON.Contains(i))
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        diagramFixture.SetBorderBrush(i, Brushes.Gray);

                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        diagramFixture.SetBorderBrush(i, Brushes.LightGray);
                    });
                }
               
            }
            showSN();
        }
        private void showSN()
        {
            List<int> listLocation = new List<int>(5) { 0, 1, 2, 3, 4 };
            for(int i = 0; i < _ListLink.Count; i++)
            {
                this.Dispatcher.Invoke(() => {
                    diagramFixture.SetLabelFixture(_ListLink[i].FixtureLocation, "SN", _ListLink[i].SN);
                });
                listLocation.RemoveAt(_ListLink[i].FixtureLocation);
            }
            foreach(int j in listLocation)
            {
                this.Dispatcher.Invoke(() => {
                    diagramFixture.SetLabelFixture(j, "SN", "");
                });
            }
        }
        public int ConvertToJson(Object _object,string name)
        {
            string savePath = $"{Const.ModelPath}";
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            string configPath = $"{savePath}/{name}.json";
            string js = JsonConvert.SerializeObject(_object);
            File.WriteAllText(configPath, js);
            return 0;
        }
        private string getModelType()   // get  model type from SFIS
        {
            string modelName = cbModelsName.Text;
            string modelType = "";
            if (modelName.Contains("PRO"))
                modelType = "PRO";
            else if (modelName.Contains("LR"))
                modelType = "LR";
            else if (modelName.Contains("LT"))
                modelType =  "LT";
            return modelType;
        }
        private void Print(string data)
        {
            int res = _Cs6.LoadDesign(_Param.NAME_PRINTER_1, @_Param.PATH_DESIGN_CODE_2D);
            int res2 = _Cs6.SetData("Barcode1", data);
            _Cs6.Print(1);
            int res3 = _Cs6.LoadDesign(_Param.NAME_PRINTER_2, @_Param.PATH_DESIGN_CODE_1D);
            int res4 = _Cs6.SetData("Barcode1", data);
            _Cs6.Print(1);
        }
        public void ShowImfor(string content, System.Windows.Media.Brush color)
        {
            this.Dispatcher.Invoke(() =>
            {
                mdiagram_main.SetTextBlock(0, 0, content);
                mdiagram_main.SetColor(0, 0, color);

            });
        }
        private void LoadCondif()
        {
            TriggerConfig = Config.Triggers.LoadModel();
        }
        private void RefreshModels()
        {
            string selected = Convert.ToString(cbModelsName.SelectedItem);
            cbModelsName.SelectedItem = -1;
            cbModelsName.Items.Clear();
            string[] modelNames = Model.Model.GetModelNames();
            if (modelNames != null)
            {
                for (int i = 0; i < modelNames.Length; i++)
                {
                    cbModelsName.Items.Add(modelNames[i]);
                }
            }
            if (modelNames.Contains(selected))
            {
                cbModelsName.SelectedItem = selected;
            }
        }
        private void mdiagram_PClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void btAbout_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btSettingClick(object sender, RoutedEventArgs e)
        {

        }

        private void btMachineIssue_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btAlarm_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btModelManager_Click(object sender, RoutedEventArgs e)
        {
            if (IsRunning == true)
            {
                MessageBoxResult res = MessageBox.Show("Please stop program before set up model !\n(hãy dừng chương trình trước khi set up hàng !)", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            if (loginWindow.UserType == UserType.Admin || loginWindow.UserType == UserType.Engineer)
            {
                DashBoard dashBoard = new DashBoard();
                dashBoard.ShowDialog();
                RefreshModels();
                //RefreshSubModels();
            }
        }
        
        private void showInforModel()
        {
            //ComboBoxItem type_item = (ComboBoxItem)cbModelsName.SelectedItem;
            try
            {
                this.Dispatcher.Invoke(() =>
                {
                    var type_item = cbModelsName.SelectedValue;

                    if (type_item != null)
                    {
                        string value = type_item.ToString();
                        lbModelName.Content = value;
                        cbModelStatistical.SelectedIndex = cbModelsName.SelectedIndex;
                    }
                });
            }
            catch
            {

            }
        }
        private int LoadUI()
        {
            int resCamera = CheckCamera();
            int resLight = CheckLight();
            int resScaner = CheckScaner();
            if (resCamera == -1 || resLight == -1 || resScaner==-1)
                return -1;
            else
                return 1;
        }
        private int CheckCamera()
        {
            string[] nameCameras = Devices.Camera.GetCameraNames();
            List<int> listResult = new List<int>();
            string mess = "Không thể kết nối đến ";
            for (int i = 0; i < nameCameras.Length; i++)
            {
                SDK.HikCamera camera = Devices.Camera.GetInstance(nameCameras[i]);
                if (camera == null)
                {
                    listResult.Add(-1);
                    mess += nameCameras[i];
                }   
                else
                    listResult.Add(1);
            }
            if (listResult.Contains(-1))
            {
                MessageBox.Show(mess, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            else
                return 1;
        }
        public int GetPing(string ip)
        {
            Ping p = new Ping();
            PingReply res;
            try
            {
                //for(int i = 0; i < 5; i++)
                //{
                 res = p.Send(ip);
                //}
            }
            catch
            {
                return -1;
            }
            return res.Status == IPStatus.Success ? 1 : -1;
        }
        private int checkPrinters(int idPrinter)
        {
            string ipPrinter = "";
            switch (idPrinter)
            {
                case 1:
                    ipPrinter = _Param.IP_PRINTER_1;
                    break;
                case 2:
                    ipPrinter = _Param.IP_PRINTER_2;
                    break;

            }
            int res = GetPing(ipPrinter);
            if (res != 1)
                MessageBox.Show($"Không thể kết nối với máy in {idPrinter}\nCant connect to printter {idPrinter}");
            return res;
        }
        private int CheckScaner()
        {
            if (_Scanner != null)
            {
                int res = _Scanner.Connect(_Param.SCANNER_PORT);
                if (res == -1)
                {
                    MessageBox.Show("Không thể kết nối với scanner", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    return -1;
                }
                else
                    return 1;
            }
            return -1;
        }
        private int CheckLight(int timeOut=500)
        {
            Devices.Light.DKZ2 light = Devices.Light.DKZ2.GetInstance();
            int res = light.Connect(_Param.LIGHT_PORT);
            if (res != 0)
            {
                MessageBox.Show("Không thể kết nối với đèn\nCant connect to light", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            else
                return 1;
        }
        private void StopRunMode()
        {
            ShowStatusMachine(0);
            btRun.Content = btRun.Content == FindResource("Play") ? FindResource("Stop") : FindResource("Play");
            lbMachineStatus.Content = "Offline";
            bdMachineStatus.Background = System.Windows.Media.Brushes.Gray;
            mdiagram_main.Visibility = Visibility.Hidden;
            imb_imgCapture.Visibility = Visibility.Hidden;
            lbLoadTime.Content = "-----";
            lbCycleTime.Content = "-----";
            lbEndTime.Content = "-----";
            IsRunning = false;
        }

        private void StartRunMode()
        {
            
            string model_name = cbModelsName.Text;

            if (model_name == "")
            {
                MessageBox.Show("Please choose model !\n(Hãy chọn model !)", "Infomation", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            int res = LoadUI();
            if (res == -1)
                return;
            Service.ReadCode.StartService();
            LoadCondif();
            showInforModel();

            //string modelType = getModelType();
            SetModelType(model_name);

            model = Model.Model.LoadModel(model_name);
            ShowStatusMachine(1);
            mdiagram_main.RenderMain(1, 1);
            mdiagram_main.Visibility = Visibility.Visible;
            IsRunning = true;
            _TimerMain.Elapsed += OnTimerScan;
            _TimerMain.Enabled = true;
            _TimerReadSN.Elapsed += OnTimerReadSN;
            _TimerReadSN.Enabled = true;
            _TimerScanA.Elapsed += OnTimerScanA;
            _TimerScanA.Enabled = true;
            //int qty_trigger = TriggerConfig.ListTriggers.Count;
            //for (int i = 0; i < qty_trigger; i++)
            //{
            //    Processing processing = new Processing(this, i);
            //    Thread.Sleep(50);
            //}
            InitThreadsProcessing();

            btRun.Content = btRun.Content == FindResource("Play") ? FindResource("Stop") : FindResource("Play");
        }
        private void InitThreadsProcessing()
        {
            int qty_trigger = TriggerConfig.ListTriggers.Count;
            Views.WaitingForm wait = new Views.WaitingForm($"Init ...", 2000);
            Thread loadingThread = new Thread(() =>
            {
                for (int i = 0; i < qty_trigger; i++)
                {
                    Processing processing = new Processing(this, i);
                    Thread.Sleep(50);
                }
                wait.KillMe = true;
            });
            loadingThread.Start();
            
        }
        private void ShowStatusMachine(int status)
        {
            switch (status)
            {

                case 0:
                    this.Dispatcher.Invoke(() =>
                    {
                        bdMachineStatus.Background = Brushes.Orange;
                        lbMachineStatus.Content = "Waiting";
                    });
                    break;
                case 1:
                    this.Dispatcher.Invoke(() =>
                    {
                        bdMachineStatus.Background = Brushes.Green;
                        lbMachineStatus.Content = "Runing";
                    });
                    break;

            }
        }
         private void btStart(object sender, RoutedEventArgs e)
        {
            if (IsRunning)
                StopRunMode();
            else
                StartRunMode();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            System.Threading.Thread.Sleep(200);
            Environment.Exit(0);

        }
        private void bt_TriggerConfig_Click(object sender, RoutedEventArgs e)
        {
            if (IsRunning == true)
            {
                MessageBoxResult res = MessageBox.Show("Please stop program before set up model !\n(hãy dừng chương trình trước khi set up hàng !)", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            
            Views.SettingWindows.TriggerConfig window = new Views.SettingWindows.TriggerConfig();
            window.ShowDialog();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine("click ok");
        }
        private void SetModelType(string modelName)
        {
            if (modelName.Contains("PRO"))
                _Status.SelectModelType(0, 0, 0);
            else if (modelName.Contains("LR"))
                _Status.SelectModelType(1, 1, 0);
            else
                _Status.SelectModelType(1, 0, 1);

        }
        public void ShowImgCapture(Image<Bgr,byte>img)
        {
            this.Dispatcher.Invoke(() =>
            {
                imb_imgCapture.Source = ToBitmapSource(img);

            });
        }
        private void btSetting_Click(object sender, RoutedEventArgs e)
        {
            if (IsRunning == true)
            {
                MessageBoxResult res = MessageBox.Show("Please stop program before set up model !\n(hãy dừng chương trình trước khi set up hàng !)", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            if (loginWindow.UserType == UserType.Admin || loginWindow.UserType == UserType.Engineer)
            {
                Views.SettingWindows.DefaultConfigs defaultConfigs = new Views.SettingWindows.DefaultConfigs();
                defaultConfigs.ShowDialog();
                RefreshModels();
                //RefreshSubModels();
            }
            
        }
    }
    public class Processing
    {
        private System.Timers.Timer _TimerProcessing = new System.Timers.Timer(200);
        private int _Index = 0;
        private MainWindow _Main = null;
       
        private Model.ObjectLocation _ObjectLocation = null;
        private SDK.HikCamera _Camera = null;
        private Config.Trigger _Trigger = null;
        private Devices.Light.DKZ2 _Light = Devices.Light.DKZ2.GetInstance();
        
        private Devices.PLC.Action.PLC2 _PLC2 = new Devices.PLC.Action.PLC2();
        private Devices.PLC.Action.PLC1 _PLC1 = new Devices.PLC.Action.PLC1();
        private Devices.PLC.Config.RegOffset _RegOffsets = new Devices.PLC.Config.RegOffset();
        private Properties.Settings _Param = Properties.Settings.Default;
        private bool _IsOnLight = false;
        public Processing(MainWindow main,int index)
        {
            _Main = main;
            _Index = _Main.TriggerConfig.ListTriggers[_Index].ObjectTarget;
            _Index = index;
            _ObjectLocation = _Main.model.Labels[_Index];
            _Trigger = _Main.TriggerConfig.ListTriggers[_Index];
            _Camera = Devices.Camera.GetInstance(_Trigger.NameCamera);
            ConnetCamera();
            ConnectLight();
            _TimerProcessing.Elapsed += OnTimeProcessing;
            _TimerProcessing.Enabled = true;
        }

        private void OnTimeProcessing(object sender, System.Timers.ElapsedEventArgs e)
        {
            _TimerProcessing.Enabled = false;
            var res = _Main.PLC2.GetDevice(_Main.TriggerConfig.ListTriggers[_Index].BitName);
            AdjustLight();
            if (res.Value == 1)
            {
                _Main.PLC2.SetDevice(_Main.TriggerConfig.ListTriggers[_Index].BitName, 0);
             
                if (_Trigger.Action == "Locate")
                {
                    int resSet = ApplyHWSettings(_ObjectLocation.Locate.AOI, _Trigger.Channel);
                    if (resSet != 1)
                    {
                        _Main.PLC2.SetDevice(_Trigger.BitFail, 1);
                        _Main.ShowImfor("Lỗi điều khiển đèn",Brushes.Red);
                    }
                }
                else
                {
                    int resSet = ApplyHWSettings(_ObjectLocation.Inspection.AOI, _Trigger.Channel);
                    if (resSet != 1)
                    {
                        _Main.PLC2.SetDevice(_Trigger.BitFail, 1);
                        _Main.ShowImfor("Lỗi điều khiển đèn", Brushes.Red);
                    }
                }
                Thread.Sleep(200);
                System.Drawing.Bitmap bm = null;
                lock(_Camera)
                {
                    bm = _Camera.GetOneBitmap(1000);
                }
                if (bm != null)
                {
                    using (Image<Bgr, byte> image = new Image<Bgr, byte>(bm))
                    {
                        CvInvoke.Imwrite($"captureCam{_Index}.jpg", image);
                        _Main.ShowImgCapture(image);
                        if (_Trigger.Action == "Locate")
                        {
                            _Main.ShowImfor($"Locate location {_Index + 1}", Brushes.Yellow);
                            _Main.ShowImfor($"Định vị label {_Index + 1}\nLocate label {_Index + 1}", Brushes.Yellow);
                            Type type = Type.GetType($"Stick_Label_F16.Algorithm.Locate.{_ObjectLocation.Locate.NameAlgorithmLocating}");
                            IProcessing instance = (IProcessing)Activator.CreateInstance(type);
                            ResultLocation result = new ResultLocation();
                            result = instance.Locate(image, _ObjectLocation.Locate.Property, _ObjectLocation.Locate.MarkPoint, _ObjectLocation.Locate.Angle, _Trigger);
                            result.Index = _Index;
                            if (result.R_Offet > 45)
                            {
                                _Main.PLC2.SetDevice(_Trigger.BitFail, 1);
                                _Main.ShowImfor($"Label {_Index + 1} lệch quá lớn\nlabel {_Index + 1} is deviated big", Brushes.Red);
                            }
                            else
                            {

                                if (_Trigger.IsLink && _Trigger.Type == 0)
                                {
                                    _Main.ListResultLocate[_Index - 3] = result;
                                    _Main.PLC2.SetDevice(_Trigger.BitPass, 1);
                                    _Main.ShowImfor($"Complete location positioning {_Index + 1}", Brushes.Green);
                                }
                                else if (!_Trigger.IsLink && _Trigger.Type == 0) // type = 0 la label 
                                {
                                    Reg regs = SelectRegOffset(_Index);
                                    int resR = _PLC2.SendCoorOffset(regs.Rp, regs.Rn, (int)(result.R_Offet * _Param.ANGLE_PULSE));
                                    int resX = _PLC2.SendCoorOffset(regs.Xp, regs.Xn, (int)(-result.X_Offset / _Param.PIXEL_MM_C1 * _Param.MM_PULSE));
                                    int resY = _PLC2.SendCoorOffset(regs.Yp, regs.Yn, (int)(-result.Y_Offset / _Param.PIXEL_MM_C1 * _Param.MM_PULSE));
                                    if (resR == -1 || resX == -1 || resY == -1)
                                        _Main.ShowImfor($"Không thể gửi tọa độ bù cho PLC2", Brushes.Red);
                                    else
                                    {
                                        _Main.ShowImfor($"Complete location positioning {_Index + 1}", Brushes.Green);
                                        _Main.PLC2.SetDevice(_Trigger.BitPass, 1);
                                    }
                                    
                                }
                                else if (_Trigger.Type == 1 && _Main.ListResultLocate.Count > 0)  // type=1 la san pham
                                {
                                    // quy doi ra mm sau khi dinh vi san pham
                                    double x_offsetProduct = result.X_Offset / _Param.PIXEL_MM_C2_2;
                                    double y_offsetProduct = result.Y_Offset / _Param.PIXEL_MM_C2_2;
                                   
                                    if (result.Status == Status.OK)
                                    {
                                        foreach (var resLocate in _Main.ListResultLocate)
                                        {
                                            // cong voi dinh vi label truoc khi dan se duoc toa do bu gui den plc
                                            if (resLocate.Index != -1)
                                            {
                                                Reg regs = SelectRegOffset(resLocate.Index);
                                                double r_offset = result.R_Offet + resLocate.R_Offet;
                                                double x_offset = -x_offsetProduct - (resLocate.X_Offset / _Param.PIXEL_MM_C2);
                                                double y_offset = -y_offsetProduct + (resLocate.Y_Offset / _Param.PIXEL_MM_C2);
                                                // gui toa do den plc
                                                int resR = _PLC2.SendCoorOffset(regs.Rp, regs.Rn, (int)(r_offset * _Param.ANGLE_PULSE));
                                                int resX = _PLC2.SendCoorOffset(regs.Xp, regs.Xn, (int)(x_offset * _Param.MM_PULSE));
                                                int resY = _PLC2.SendCoorOffset(regs.Yp, regs.Yn, (int)(y_offset * _Param.MM_PULSE));
                                                if (resR == -1 || resX == -1 || resY == -1)
                                                    _Main.ShowImfor($"Không thể gửi tọa độ bù cho PLC2", Brushes.Red);
                                            }
                                            else
                                                continue;
                                        }
                                        _Main.ShowImfor($"Complete location positioning {_Index + 1}", Brushes.Green);
                                        _Main.PLC2.SetDevice(_Trigger.BitPass, 1);
                                    }
                                    else
                                    {
                                        _Main.ShowImfor($"Không tìm thấy template", Brushes.Red);
                                        _Main.PLC2.SetDevice(_Trigger.BitFail, 1);
                                    }
                                }
                            }

                        }
                        else
                        {
                            _Main.ShowImfor($"Inspecting...", Brushes.Yellow);
                            Type typeLocate = Type.GetType($"Stick_Label_F16.Algorithm.Locate.{_ObjectLocation.Locate.NameAlgorithmLocating}");
                            IProcessing instanceLocate = (IProcessing)Activator.CreateInstance(typeLocate);
                            ResultLocation offsetValue = new ResultLocation();
                            offsetValue = instanceLocate.Locate(image, _ObjectLocation.Locate.Property, _ObjectLocation.Locate.MarkPoint, _ObjectLocation.Locate.Angle, _Trigger);

                            Type type = Type.GetType($"Stick_Label_F16.Algorithm.Inspection.{_ObjectLocation.Inspection.NameAlgorithm}");
                            IProcessing instance = (IProcessing)Activator.CreateInstance(type);
                            List<bool> resultInspection = instance.Inspect(image.Bitmap, _ObjectLocation.Inspection.Property, "aaa", offsetValue);
                            if (!resultInspection.Contains(false))
                            {
                                var resSet = _Main.PLC2.SetDevice(_Trigger.BitPass, 1);
                                _Main.ShowImfor("Stick label successful\n Dấn label thành công ", Brushes.Green);
                            }
                            else
                            {
                                _Main.PLC2.SetDevice(_Trigger.BitFail, 1);
                                _Main.ShowImfor("Label is sticked fail\n Label dán lỗi", Brushes.Red);
                            }

                        }
                    }
                }
                else
                    _Main.ShowImfor($"{_Trigger.NameCamera} cant take photo",Brushes.Red);
            }
            Thread.Sleep(200);
            _TimerProcessing.Enabled = _Main.IsRunning;
        }
        private void AdjustLight()
        {
            var res = _Main.PLC2.GetDevice(_Main._Status.BIT_ON_LIGHT);
            if (res.Value == 1 && _IsOnLight==false)
            {
                _Light.SetOne(1, 255);
                _Light.SetOne(2, 255);
                _Light.SetOne(4, 255);
                _IsOnLight = true;
            }
            if (res.Value == 0 && _IsOnLight == true)
            {
                _Light.SetOne(1, 0);
                _Light.SetOne(2, 0);
                _Light.SetOne(4, 0);
                _IsOnLight = false;
            }
        }
        private Reg SelectRegOffset(int index)
        {
            Reg reg = new Reg();
            switch (index)
            {
                case 0:
                    reg.Rp = _RegOffsets.R_OFFSET_PICKUP_POS_1;
                    reg.Rn = _RegOffsets.R_OFFSET_PICKUP_NEG_1;
                    reg.Xp = _RegOffsets.X_OFFSET_PICKUP_POS_1;
                    reg.Xn = _RegOffsets.X_OFFSET_PICKUP_NEG_1;
                    reg.Yp = _RegOffsets.Y_OFFSET_PICKUP_POS_1;
                    reg.Yn = _RegOffsets.Y_OFFSET_PICKUP_NEG_1;
                    break;
                case 1:
                    reg.Rp = _RegOffsets.R_OFFSET_PICKUP_POS_2;
                    reg.Rn = _RegOffsets.R_OFFSET_PICKUP_NEG_2;
                    reg.Xp = _RegOffsets.X_OFFSET_PICKUP_POS_2;
                    reg.Xn = _RegOffsets.X_OFFSET_PICKUP_NEG_2;
                    reg.Yp = _RegOffsets.Y_OFFSET_PICKUP_POS_2;
                    reg.Yn = _RegOffsets.Y_OFFSET_PICKUP_NEG_2;
                    break;
                case 2:
                    reg.Rp = _RegOffsets.R_OFFSET_PICKUP_POS_3;
                    reg.Rn = _RegOffsets.R_OFFSET_PICKUP_NEG_3;
                    reg.Xp = _RegOffsets.X_OFFSET_PICKUP_POS_3;
                    reg.Xn = _RegOffsets.X_OFFSET_PICKUP_NEG_3;
                    reg.Yp = _RegOffsets.Y_OFFSET_PICKUP_POS_3;
                    reg.Yn = _RegOffsets.Y_OFFSET_PICKUP_NEG_3;
                    break;
                case 3:
                    reg.Rp = _RegOffsets.R_OFFSET_STICK_POS_1;
                    reg.Rn = _RegOffsets.R_OFFSET_STICK_NEG_1;
                    reg.Xp = _RegOffsets.X_OFFSET_STICK_POS_1;
                    reg.Xn = _RegOffsets.X_OFFSET_STICK_NEG_1;
                    reg.Yp = _RegOffsets.Y_OFFSET_STICK_POS_1;
                    reg.Yn = _RegOffsets.Y_OFFSET_STICK_NEG_1;
                    break;
                case 4:
                    reg.Rp = _RegOffsets.R_OFFSET_STICK_POS_2;
                    reg.Rn = _RegOffsets.R_OFFSET_STICK_NEG_2;
                    reg.Xp = _RegOffsets.X_OFFSET_STICK_POS_2;
                    reg.Xn = _RegOffsets.X_OFFSET_STICK_NEG_2;
                    reg.Yp = _RegOffsets.Y_OFFSET_STICK_POS_2;
                    reg.Yn = _RegOffsets.Y_OFFSET_STICK_NEG_2;
                    break;
                case 5:
                    reg.Rp = _RegOffsets.R_OFFSET_STICK_POS_3;
                    reg.Rn = _RegOffsets.R_OFFSET_STICK_NEG_3;
                    reg.Xp = _RegOffsets.X_OFFSET_STICK_POS_3;
                    reg.Xn = _RegOffsets.X_OFFSET_STICK_NEG_3;
                    reg.Yp = _RegOffsets.Y_OFFSET_STICK_POS_3;
                    reg.Yn = _RegOffsets.Y_OFFSET_STICK_NEG_3;
                    break;
            }
            return reg;
        }
        private int ConnetCamera()
        {
            if (_Camera != null)
            {
                int res = _Camera.Open();
                if (res == 0)
                {
                    int resGrab = _Camera.StartGrabbing();
                    return resGrab == 0 ? 1 : -1;
                }
                else
                    return -1;
            }
            else
                return -1;
        }
        private int ConnectLight()
        {
            if (_Light != null)
            {
                int res = _Light.Connect(_Param.LIGHT_PORT);
                return res == 0 ? 1 : -1;
            }
            else
                return -1;
        }
        private int ApplyHWSettings(Model.AOI aoi, int channel)      // index camera truyen vao bang 0 la khi dinh vi label
        {
            int result = -1;
            if (_Camera != null)
            {
                _Camera.SetParameter(SDK.KeyName.ExposureTime, aoi.ExposureTime);
                _Camera.SetParameter(SDK.KeyName.Gamma, aoi.Gamma);
                if (_Light != null)
                {
                    int res = _Light.SetOne(channel, aoi.CH);
                    result = res == 1 ? 1 : -1;
                    
                }
            }
            return result;
        }
        private void TurnOffLight(int channel)
        {
            var res = _Main.PLC2.GetDevice(_Main._Status.BIT_ON_LIGHT);
            if (res.Value == 0 && _IsOnLight == true)
            {
                if (_Light != null)
                {
                    _Light.SetOne(channel, 0);

                }
            }
            
        }
    }
    public class Link 
    {
        public string ScarierCode { get; set; }
        public int FixtureLocation { get; set; }
        public string SN { get; set; }
    }
    public class Reg
    {
        public string Rn { get; set; }
        public string Rp { get; set; }
        public string Xp { get; set; }
        public string Xn { get; set; }
        public string Yp { get; set; }
        public string Yn { get; set; }
    }
}
