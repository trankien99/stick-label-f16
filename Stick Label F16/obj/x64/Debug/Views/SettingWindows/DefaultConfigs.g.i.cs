﻿#pragma checksum "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "41C30006BB8608FA1B3B560F1E11463768D4F12BF6DE72D95D7797D312174FA2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Stick_Label_F16.Views.SettingWindows;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Stick_Label_F16.Views.SettingWindows {
    
    
    /// <summary>
    /// DefaultConfigs
    /// </summary>
    public partial class DefaultConfigs : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_ipPlc1;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_portPlc1;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_ipPlc2;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_portPlc2;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_ipPrinter1;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_fileDesign1;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_ipPrinter2;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_fileDesign2;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_tolerantXY;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_toleranceAngle;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_pixelToMm1;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_pixelToMm2;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_pixelToMm3;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_portLight;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_mmToPulse;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_angleToPulse;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_portScaner;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Stick Label F16;component/views/settingwindows/defaultconfigs.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            ((Stick_Label_F16.Views.SettingWindows.DefaultConfigs)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.Window_Closing);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txt_ipPlc1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 33 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_ipPlc1.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_ipPlc1_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.txt_portPlc1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 37 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_portPlc1.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_portPlc1_TextChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.txt_ipPlc2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 42 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_ipPlc2.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_ipPlc2_TextChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txt_portPlc2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 46 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_portPlc2.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_portPlc2_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.txt_ipPrinter1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 57 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_ipPrinter1.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_ipPrinter1_TextChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.txt_fileDesign1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 61 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_fileDesign1.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_fileDesign1_TextChanged);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 62 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.bt_selectFile1);
            
            #line default
            #line hidden
            return;
            case 9:
            this.txt_ipPrinter2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 70 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_ipPrinter2.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_ipPrinter2_TextChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.txt_fileDesign2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 74 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_fileDesign2.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_fileDesign2_TextChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 75 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.bt_selectFile2);
            
            #line default
            #line hidden
            return;
            case 12:
            this.txt_tolerantXY = ((System.Windows.Controls.TextBox)(target));
            
            #line 88 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_tolerantXY.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_tolerantXY_TextChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.txt_toleranceAngle = ((System.Windows.Controls.TextBox)(target));
            
            #line 92 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_toleranceAngle.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_toleranceAngle_TextChanged);
            
            #line default
            #line hidden
            return;
            case 14:
            this.txt_pixelToMm1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 113 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_pixelToMm1.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_pixelToMm1_TextChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            this.txt_pixelToMm2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 118 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_pixelToMm2.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_pixelToMm2_TextChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.txt_pixelToMm3 = ((System.Windows.Controls.TextBox)(target));
            
            #line 123 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_pixelToMm3.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_pixelToMm3_TextChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.txt_portLight = ((System.Windows.Controls.TextBox)(target));
            
            #line 133 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_portLight.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_portLight_TextChanged);
            
            #line default
            #line hidden
            return;
            case 18:
            this.txt_mmToPulse = ((System.Windows.Controls.TextBox)(target));
            
            #line 143 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_mmToPulse.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_mmToPulse_TextChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.txt_angleToPulse = ((System.Windows.Controls.TextBox)(target));
            
            #line 147 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_angleToPulse.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_angleToPulse_TextChanged);
            
            #line default
            #line hidden
            return;
            case 20:
            this.txt_portScaner = ((System.Windows.Controls.TextBox)(target));
            
            #line 157 "..\..\..\..\..\Views\SettingWindows\DefaultConfigs.xaml"
            this.txt_portScaner.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_portScaner_TextChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

