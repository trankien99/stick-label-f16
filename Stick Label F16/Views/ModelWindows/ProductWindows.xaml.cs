﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Stick_Label_F16.Model;
using Stick_Label_F16.Algorithm.UIProperties;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;

namespace Stick_Label_F16.Views.ModelWindows
{
    /// <summary>
    /// Interaction logic for ProductWindows.xaml
    /// </summary>
    
    public partial class ProductWindows : Window, INotifyPropertyChanged
    {
        public Model.ObjectLocation _ObjectLocation = null;
        public Model.Model _Model = null;
        public int _IdLabel = 0;
        public System.Drawing.Rectangle RoiLocate = new System.Drawing.Rectangle();
        public System.Drawing.Rectangle RoiInspection = new System.Drawing.Rectangle();
        public System.Drawing.Bitmap bm = null;
        public bool IsSetNewTemplate = false;
        public TabAction _TabAction = TabAction.locate;
        public int IndexInspectionSelected = -1;

        private int _IdCamera = 0;
        private Properties.Settings _Param = Properties.Settings.Default;
        
        
        public bool Draw = false;
        private bool _Run = true;
        private System.Timers.Timer _Timer = new System.Timers.Timer(50);
        public Config.Triggers Triggers = null;
        private List<string> _ListLight = new List<string>();
        public SDK.HikCamera Camera = null;    // camera 1 tay 1

        private Devices.Light.DKZ2 Light= null;
        public event PropertyChangedEventHandler PropertyChanged;
        public Model.ObjectLocation mObjectLocation
        {
            get
            {
                return _ObjectLocation;
            }
            set
            {
                _ObjectLocation = value;
                OnPropertyChanged();
               
            }
        }
        public ProductWindows(Model.Model model,int location)
        {
            
            _Model = model;
            _ObjectLocation = model.Labels[location].Copy();
            _IdLabel = location;
            Triggers = Config.Triggers.LoadModel();
            //Camera = Devices.Camera.GetInstance(Triggers.ListTriggers[location].NameCamera);
            //Light = Devices.Light.DKZ2.GetInstance();
            InitializeComponent();
            int res = LoadUI();
            if (res == 1)
            {
                this.DataContext = this;
                addItemsCombobox(_Param.PATH_ALGORITHM_LOCATE);
                addItemsCombobox(_Param.PATH_ALGORITHM_INSPECTION);
            }
            else
                this.IsEnabled = false;
        }
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        
        private int LoadUI()
        {
            //int res = CheckCamera();
            //int resLight = CheckLight();
            //if (res != 1 || resLight != 1)
            //    return -1;
            //else
            //    return 1;
            return 1;
        }
        private int CheckCamera()
        {
            if (Camera != null)
            {
                Camera.Open();
                if (Camera.IsOpen)
                {
                    Camera.StartGrabbing();
                    if (Camera.IsGrab)
                        return 1;
                    else
                        return -1;
                }
                else
                {
                    MessageBox.Show("Không thể mở camera", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                    return -1;
                }
                    
            }
            else
            {
                MessageBox.Show("Cant connect to camera");
                
                return -1;
            }
        }
        private int CheckLight(int timeOut = 500)
        {
            Devices.Light.DKZ2 light = Devices.Light.DKZ2.GetInstance();
            int res = light.Connect(_Param.LIGHT_PORT);
            if (res != 0)
            {
                MessageBox.Show("Không thể kết nối với đèn\nCant connect to light", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            else
                return 1;
        }
        public void ApplyHWSettings(AOI aoi,int channel)      // index camera truyen vao bang 0 la khi dinh vi label
        {
            if (Camera != null)
            {
                Camera.SetParameter(SDK.KeyName.ExposureTime, aoi.ExposureTime);
                Camera.SetParameter(SDK.KeyName.Gamma, aoi.Gamma);
                if (Light != null)
                {
                    Light.SetOne(channel, aoi.CH);

                }
            }
            
        }
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tab = sender as TabControl;

            switch (tab.SelectedIndex)
            {
                case 0:
                    _TabAction = TabAction.locate;
                    _IdCamera = 0;
                    //ApplyHWSettings(_ObjectLocation.Locate.AOI, Triggers.ListTriggers[_IdLabel].Channel);
                    break;
                case 1:
                    _TabAction = TabAction.inspection;
                    //ApplyHWSettings(_ObjectLocation.Inspection.AOI, Triggers.ListTriggers[_IdLabel].Channel);
                    break;
                default:
                    break;
            }
        }
        #region LocateLabel
        private void txtExposureTime_Label_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }

            mObjectLocation.Locate.AOI.ExposureTime = value;
            OnPropertyChanged();
            //ApplyHWSettings(mObjectLocation.Locate.AOI, Triggers.ListTriggers[_IdLabel].Channel);
        }

        private void txtGamma_Label_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double value = 0;
            try
            {
                value = Convert.ToDouble(txt.Text);
            }
            catch { return; }
            mObjectLocation.Locate.AOI.Gamma = value;
            OnPropertyChanged();
            //ApplyHWSettings(mObjectLocation.Locate.AOI, Triggers.ListTriggers[_IdLabel].Channel);
        }

        private void txtLightCH1_Label_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }
            mObjectLocation.Locate.AOI.CH = value;
            OnPropertyChanged();
            //ApplyHWSettings(mObjectLocation.Locate.AOI, Triggers.ListTriggers[_IdLabel].Channel);
        }
        private void addItemsCombobox(string path)
        {
            string[] path_folders = Directory.GetFiles(@path, "*.cs");
            foreach(string path_folder in path_folders)
            {
                string file_name = System.IO.Path.GetFileName(System.IO.Path.GetFileName(path_folder)).Split('.')[0];
                if (path == _Param.PATH_ALGORITHM_LOCATE)
                    algorithms.Items.Add(file_name);
                else
                    cb_algrithm_inspections.Items.Add(file_name);

            }
                
        }
        //private void Window_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        //{
        //    if (_DrawMode != DrawMode.None)
        //    {
        //        Rect rect = imb.GetSelectRectangle();
        //        if (rect != Rect.Empty)
        //        {
        //            var rectInt = new System.Drawing.Rectangle(
        //                       Convert.ToInt32(rect.X),
        //                       Convert.ToInt32(rect.Y),
        //                       Convert.ToInt32(rect.Width),
        //                       Convert.ToInt32(rect.Height));

        //            if (_DrawMode == DrawMode.Inspection)
        //            {
        //                _Label.Inspection.ROI = rectInt;
        //            }
                    
        //            //if (_DrawMode == DrawMode.)
        //            //{
        //            //    _Label.Locate.ROI = rectInt;
        //            //    mIsSetNewTemplate = true;
        //            //}
                    
        //            _DrawMode = DrawMode.None;
        //            imb.ResetDrawRectMode();
        //            OnPropertyChanged();
        //        }
        //    }
        //}
        private void SaveModel()
        {
            int res = _Model.Save();
            if (res == 0)
                MessageBox.Show("Save model successfully !", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        #endregion
        private void btSave_Label_Click(object sender, RoutedEventArgs e)
        {
            SaveModel();
        }
        private void diagram_PClick(object sender, MouseButtonEventArgs e)
        {
            Grid g = sender as Grid;
            if (g != null)
            {
                int col = Grid.GetColumn(g);
                int row = Grid.GetRow(g);
                int cols = diagram.Cols;
                int index = row * cols + col;
                IndexInspectionSelected = index;
                for (int i = 0; i < Convert.ToInt32(qty_labels.Text); i++)
                {
                    if (i == index)
                        diagram.SetColor(index, 0, System.Windows.Media.Brushes.White);
                    else
                        diagram.SetColor(i, 0, System.Windows.Media.Brushes.LightGray);
                }
                string algorithm = cb_algrithm_inspections.SelectedItem.ToString();
                Type type = Type.GetType($"Stick_Label_F16.Algorithm.UIProperties.{algorithm}");
                property_inpection.Children.Clear();
                if (type != null)
                {
                    IUI instance = (IUI)Activator.CreateInstance(type, this, index);
                    instance.InitPropertyUi();
                }
            }
        }
        private void txtExposureTime_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }

            mObjectLocation.Inspection.AOI.ExposureTime = value;
            OnPropertyChanged();
            //ApplyHWSettings(mObjectLocation.Inspection.AOI, Triggers.ListTriggers[_IdLabel].Channel);
        }

        private void txtGamma_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double value = 0;
            try
            {
                value = Convert.ToDouble(txt.Text);
            }
            catch { return; }
            mObjectLocation.Inspection.AOI.Gamma = value;
            OnPropertyChanged();
            //ApplyHWSettings(mObjectLocation.Inspection.AOI, Triggers.ListTriggers[_IdLabel].Channel);
        }

        private void txtCH1_Inspection_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }
            mObjectLocation.Inspection.AOI.CH = value;
            OnPropertyChanged();
            //ApplyHWSettings(mObjectLocation.Inspection.AOI, Triggers.ListTriggers[_IdLabel].Channel);
        }

        private void algorithms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string algorithm = cb.SelectedItem.ToString();
            Type type = Type.GetType($"Stick_Label_F16.Algorithm.UIProperties.{algorithm}");
            properties_control.Children.Clear();
            if (type != null)
            {
                IUI instance = (IUI)Activator.CreateInstance(type,this);
                instance.InitPropertyUi();
            }
        }

        private void Window_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Draw)
            {
                Rect rect = imb.GetSelectRectangle();
                if (rect != Rect.Empty)
                {
                    var rectInt = new System.Drawing.Rectangle(
                               Convert.ToInt32(rect.X),
                               Convert.ToInt32(rect.Y),
                               Convert.ToInt32(rect.Width),
                               Convert.ToInt32(rect.Height));


                    if (_TabAction == TabAction.locate)
                        RoiLocate = rectInt;
                    else if (_TabAction == TabAction.inspection)
                        RoiInspection = rectInt;
                    Draw = false;
                    IsSetNewTemplate = true;
                    imb.ResetDrawRectMode();
                    OnPropertyChanged();
                }
            }
        }
        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Draw = false;
                imb.ResetDrawRectMode();
            }

        }
        private void qty_labels_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            int value = 0;
            try
            {
                value = Convert.ToInt32(txt.Text);
            }
            catch { return; }
            mObjectLocation.Inspection.QtyLabels = value;
            OnPropertyChanged();
            diagram.Render(value, 1);
        }

        private void cb_algrithm_inspections_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            property_inpection.Children.Clear();
            for(int i = 0; i < Convert.ToInt32(qty_labels.Text); i++)
            {
                diagram.SetColor(i, 0, System.Windows.Media.Brushes.LightGray);
            }
        }

        public enum TabAction
        {
            inspection,
            locate,
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if(Camera!=null)
                Camera.Close();
            Light.SetOne(Triggers.ListTriggers[_IdLabel].Channel, 0);
        }
    }
}
