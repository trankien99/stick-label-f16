﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Stick_Label_F16.Model;
using System.Net.NetworkInformation;

namespace Stick_Label_F16.Views.ModelWindows
{
    /// <summary>
    /// Interaction logic for DashBoard.xaml
    /// </summary>
    public partial class DashBoard : Window, INotifyPropertyChanged
    {
        private Model.Model _Model = null;
        private Model.Model _ModelCopy = null;
        private string _TypeModel = "";
        private Devices.PLC.Status _Status = new Devices.PLC.Status();
        private Devices.PLC.Action.PLC2 _PLC2 = new Devices.PLC.Action.PLC2();
        private Devices.PLC.Action.PLC1 _PLC1 = new Devices.PLC.Action.PLC1();
        private Properties.Settings _Param = Properties.Settings.Default;
        private Devices.Printer.CodeSoft6 _Cs6 = Devices.Printer.CodeSoft6.GetInstance();
        private bool _IsChange = false;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        public DashBoard()
        {
            InitializeComponent();
            AddNameModel();
            this.DataContext = this;
        }
        public Model.Model model
        {
            get
            {
                return _ModelCopy;
            }
            set
            {
                _ModelCopy = value;
                OnPropertyChanged();
            }
        }

        private void btRefresh_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btNewModel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            if (_IsChange)
            {
                _TypeModel = cbModelsName.SelectedItem.ToString();
                _Model = _Model = Model.Model.LoadModel(_TypeModel);
                _Model.ListEnable = _ModelCopy.ListEnable;
                if (_Model != null)
                {
                    int res =_Model.Save();
                    if (res == 0)
                        MessageBox.Show("Save model successfully", "INFOR", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {

        }
        private void AddNameModel()
        {
            cbModelsName.Items.Add(_Param.NAME_PRODUCT1);
            cbModelsName.Items.Add(_Param.NAME_PRODUCT2);
            cbModelsName.Items.Add(_Param.NAME_PRODUCT3);
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_IsChange)
            {
                _TypeModel = cbModelsName.SelectedItem.ToString();
                _Model = _Model = Model.Model.LoadModel(_TypeModel);
                _Model.ListEnable = _ModelCopy.ListEnable;
                if (_Model != null)
                    _Model.Save();
            }

            int res = _Status.LogOut();
        }
        private void SetModelType(string modelName)
        {
            if (modelName.Contains("PRO"))
                _Status.SelectModelType(0, 0, 0);
            else if (modelName.Contains("LR"))
                _Status.SelectModelType(1, 1, 0);
            else
                _Status.SelectModelType(1, 0, 1);

        }
        public int GetPing(string ip)
        {
            Ping p = new Ping();
            PingReply res;
            try
            {
                //for(int i = 0; i < 5; i++)
                //{
                res = p.Send(ip);
                //}
            }
            catch
            {
                return -1;
            }
            return res.Status == IPStatus.Success ? 1 : -1;
        }
        private int checkPrinters(int idPrinter)
        {
            string ipPrinter = "";
            switch (idPrinter)
            {
                case 1:
                    ipPrinter = _Param.IP_PRINTER_1;
                    break;
                case 2:
                    ipPrinter = _Param.IP_PRINTER_2;
                    break;

            }
            int res = GetPing(ipPrinter);
            return res;
        }
        private void cbModelsName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            if (cbModelsName.SelectedIndex > -1)
            {
                //_TypeModel = cb.SelectedItem.ToString().Split(':')[1];
                //SetModelType(_TypeModel);
                _TypeModel = cb.SelectedItem.ToString();
                SetModelType(_TypeModel);
                Views.WaitingForm wait = new WaitingForm($"Loading model: {_TypeModel}", 15);
                Thread loadingThread = new Thread(() => {
                    _Model = Model.Model.LoadModel(_TypeModel);
                    wait.KillMe = true;
                });
                loadingThread.Start();
                wait.ShowDialog();
                if (_Model == null)
                {
                    _Model = new Model.Model(_TypeModel);
                    _Model.Save();
                }
                _ModelCopy = _Model;
                OnPropertyChanged();
            } 
        }

        private void location1_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_BITLOCATION_1);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model,0);
            window.Show();
        }

        private void location6_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_BITLOCATION_4_5_6);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 5);
            window.Show();
        }

        private void location5_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_BITLOCATION_4_5_6);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 4);
            window.Show();
        }

        private void location4_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_BITLOCATION_4_5_6);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 3);
            window.Show();
        }

        private void location3_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_BITLOCATION_3);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 2);
            window.Show();
        }

        private void location2_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_BITLOCATION_2);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 1);
            window.Show();
        }

        private void bt_load_Click(object sender, RoutedEventArgs e)
        {
            //int res = _Status.Login();
            //if (res != 1)
            //    return;
            //if (_Model == null)
            //{
            //    MessageBox.Show("Model have not selected yet\nModel chưa được chọn", "WARNING", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return;
            //}
            ////_PLC2.Reset();
            ////Thread.Sleep(100);
            //_PLC2.Go(_PLC2.BIT_LOAD);
            //int res1 = _PLC1.Go(_PLC1.BIT_BITLOCATION_0);
            list_label.IsEnabled = true;
        }

        private void unload_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res = _PLC2.Go(_PLC2.BIT_UNLOAD);
        }

        private void location8_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            //int res1 = _PLC1.Go(_PLC1.BIT_BITLOCATION_0);
            int res2 = _PLC2.Go(_PLC2.BIT_BITLOCATION_7);
            

            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 7);
            window.Show();
        }

        private void location7_Click(object sender, RoutedEventArgs e)
        {
            //_PLC2.Reset();
            //Thread.Sleep(100);
            int res2 = _PLC2.Go(_PLC2.BIT_BITLOCATION_0);
            //int res1 = _PLC1.Go(_PLC1.BIT_BITLOCATION_0);
            Views.ModelWindows.ProductWindows window = new ProductWindows(_Model, 6);
            window.Show();
        }

        private void isEnable1_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[0] = true;
            }
            else
                _ModelCopy.ListEnable[0] = false;
            _IsChange = true;
            OnPropertyChanged();

        }
        private void isEnable2_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[1] = true;
            }
            else
                _ModelCopy.ListEnable[1] = false;
            _IsChange = true;
            OnPropertyChanged();

        }
        private void isEnable3_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[2] = true;
            }
            else
                _ModelCopy.ListEnable[2] = false;
            _IsChange = true;
            OnPropertyChanged();

        }
        private void isEnable4_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[3] = true;
            }
            else
                _ModelCopy.ListEnable[3] = false;
            _IsChange = true;

            OnPropertyChanged();

        }
        private void isEnable5_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[4] = true;
            }
            else
                _ModelCopy.ListEnable[4] = false;
            _IsChange = true;
            OnPropertyChanged();

        }
        private void isEnable6_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[5] = true;
            }
            else
                _ModelCopy.ListEnable[5] = false;
            _IsChange = true;
            OnPropertyChanged();

        }
        private void isEnable7_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[6] = true;
            }
            else
                _ModelCopy.ListEnable[6] = false;
            _IsChange = true;
            OnPropertyChanged();


        }
        private void isEnable8_Click(object sender, RoutedEventArgs e)
        {
            CheckBox ck = sender as CheckBox;
            if (ck.IsChecked == true)
            {
                _ModelCopy.ListEnable[7] = true;
            }
            else
                _ModelCopy.ListEnable[7] = false;
            _IsChange = true;
            OnPropertyChanged();


        }
        private void Print1(string data,string printerName)
        {
            int res = _Cs6.LoadDesign(printerName, _Param.PATH_DESIGN_CODE_2D);
            int res2 = _Cs6.SetData("Barcode1", data);
            _Cs6.Print(1);
        }
        private void Print2(string data, string printerName)
        {
            int res = _Cs6.LoadDesign(printerName, _Param.PATH_DESIGN_CODE_1D);
            int res2 = _Cs6.SetData("Barcode1", data);
            _Cs6.Print(1);
        }
        private void btPrinter1_Click(object sender, RoutedEventArgs e)
        {
            Print1("123", _Param.NAME_PRINTER_1);
        }

        private void btPrinter2_Click(object sender, RoutedEventArgs e)
        {
            Print2("123", _Param.NAME_PRINTER_2);

        }
    }
}
