﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Stick_Label_F16.Views.SettingWindows
{
    /// <summary>
    /// Interaction logic for TriggerConfig.xaml
    /// </summary>
    public partial class TriggerConfig : Window, INotifyPropertyChanged
    {
        private Config.Trigger _Config = new Config.Trigger();
        private Config.Triggers _ListConfig = null;
        private int _Stt = 0;
        private List<string> _ListCamera = new List<string>();
        public TriggerConfig()
        {
            InitializeComponent();
            _ListConfig = Config.Triggers.LoadModel();
            if (_ListConfig == null)
                _ListConfig = new Config.Triggers();
            else
                diagram.RenderTriggerConfig(1, _ListConfig);
            _Stt = _ListConfig.ListTriggers.Count+1;
            txt_Stt.Text = _Stt.ToString();
            _ListCamera = Devices.Camera.GetCameraNames().ToList();
            AddCameraCb();
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        private void AddCameraCb()
        {
            for(int i = 0; i < _ListCamera.Count; i++)
            {
                cb_camera.Items.Add(_ListCamera[i]);
            }
        }
        private void bt_add_Click(object sender, RoutedEventArgs e)
        {
            if (IsLink.IsChecked == true)
                _Config.IsLink = true;
            else
                _Config.IsLink = false;
            try
            {
                _Stt = Convert.ToInt32(txt_Stt.Text);
                _Config.BitName = txt_BitName.Text.ToUpper();
                _Config.ObjectTarget = Convert.ToInt32(txt_ObjectTarget.Text);
                _Config.Type = cb_Type.SelectedIndex;
                _Config.Action = ((ComboBoxItem)cb_Action.SelectedItem).Content.ToString();
                _Config.NameCamera = cb_camera.SelectedItem.ToString();
                string[] values = txt_centerRotation.Text.Split(',');
                _Config.CenterRotation = new System.Drawing.Point(Convert.ToInt32(values[0]), Convert.ToInt32(values[1]));
                _Config.Channel = Convert.ToInt32(txt_light.Text);
                _Config.BitPass = txt_pass.Text.ToUpper();
                _Config.BitFail = txt_fail.Text.ToUpper();
            }
            catch { };
            if (_Stt> _ListConfig.ListTriggers.Count)
            {
                _ListConfig.QtyTrigger = _Stt;
                _ListConfig.ListTriggers.Add(_Config.Copy());
                _Stt += 1;    
            }
            else
            {
                int index = Convert.ToInt32(txt_Stt.Text) - 1;
                _ListConfig.ListTriggers[index] = _Config.Copy();
                _Stt = _ListConfig.QtyTrigger +1;
                bt_add.Content = "ADD";
            }
            int res = _ListConfig.Save();
            diagram.RenderTriggerConfig(1, _ListConfig);
            clear();
            txt_Stt.Text = Convert.ToString(_Stt);
        }
        private void Binding(string stt,string bit_name,string object_target,int type,int camera,string light,bool is_link,int action,string centerRotaion,string bitPass,string bitFail)
        {
            txt_BitName.Text = bit_name;
            txt_ObjectTarget.Text = object_target;
            cb_Type.SelectedIndex = type;
            txt_Stt.Text = stt;
            cb_camera.SelectedIndex = camera;
            txt_light.Text = light;
            IsLink.IsChecked = is_link;
            cb_Action.SelectedIndex = action;
            txt_centerRotation.Text = centerRotaion;
            txt_fail.Text = bitFail;
            txt_pass.Text = bitPass;
        }
        
        private void txt_BitName_TextChanged(object sender, TextChangedEventArgs e)
        {
            //TextBox txt = sender as TextBox;
            //string value = txt.Text;
            //_Config.BitName = value.ToUpper();
            //OnPropertyChanged(); 
        }

        private void txt_ObjectTarget_TextChanged(object sender, TextChangedEventArgs e)
        {
            //TextBox txt = sender as TextBox;
            //int value = -1;
            //try
            //{
            //    value = Convert.ToInt32(txt_ObjectTarget.Text);
            //}
            //catch { };
            // _Config.ObjectTarget = value;
            //OnPropertyChanged();
        }
        private void clear()
        {
            txt_BitName.Text = null;
            txt_ObjectTarget.Text = null;
            cb_Type.SelectedItem = null;
            cb_camera.SelectedItem = null;
            txt_light.Text = null;
            txt_centerRotation.Text = null;
            cb_Action.SelectedItem = null;
            txt_pass.Text = null;
            txt_fail.Text = null;
           
        }
        private void cb_Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ComboBox cb = sender as ComboBox;
            //try
            //{
            //    string selected = ((ComboBoxItem)cb.SelectedItem).Content.ToString();
            //    _Config.Type = selected;
            //}
            //catch { };
            //OnPropertyChanged();
        }

        private void diagram_PClick(object sender, MouseButtonEventArgs e)
        {
            Grid g = sender as Grid;
            if (g != null)
            {
                int col = Grid.GetColumn(g);
                int row = Grid.GetRow(g);
                int cols = diagram.Cols;
                int index = row * cols + col;
                int indexCamera = _ListCamera.IndexOf(_ListConfig.ListTriggers[index].NameCamera);
                string centerRotaion = _ListConfig.ListTriggers[index].CenterRotation.X.ToString() + "," + _ListConfig.ListTriggers[index].CenterRotation.Y.ToString();
                if (_ListConfig.ListTriggers[index].Action == "Inspection")
                    Binding(Convert.ToString(index + 1), _ListConfig.ListTriggers[index].BitName, Convert.ToString(_ListConfig.ListTriggers[index].ObjectTarget), _ListConfig.ListTriggers[index].Type, indexCamera, _ListConfig.ListTriggers[index].Channel.ToString(), _ListConfig.ListTriggers[index].IsLink,1, centerRotaion, _ListConfig.ListTriggers[index].BitPass, _ListConfig.ListTriggers[index].BitFail);
                else
                    Binding(Convert.ToString(index + 1), _ListConfig.ListTriggers[index].BitName, Convert.ToString(_ListConfig.ListTriggers[index].ObjectTarget), _ListConfig.ListTriggers[index].Type, indexCamera, _ListConfig.ListTriggers[index].Channel.ToString(), _ListConfig.ListTriggers[index].IsLink,0, centerRotaion, _ListConfig.ListTriggers[index].BitPass, _ListConfig.ListTriggers[index].BitFail);
                bt_add.Content = "UPDATE";
            }
        }
    }
}
