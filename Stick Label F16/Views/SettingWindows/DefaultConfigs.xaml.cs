﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Stick_Label_F16.Views.SettingWindows
{
    /// <summary>
    /// Interaction logic for DefaultConfigs.xaml
    /// </summary>
    public partial class DefaultConfigs : Window, INotifyPropertyChanged
    {
        private Properties.Settings _Param = Properties.Settings.Default;
        Object ParamDefault = null;
        public DefaultConfigs()
        {
            InitializeComponent();
            this.DataContext = this;
            //ConvertToJson(_Param,"ValueDefault");
            ParamDefault = _Param;
        }
        public Object Param 
        {
            get
            {
                return ParamDefault;
            }
            set
            {
                ParamDefault = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        private void bt_selectFile1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            Nullable<bool> result = openFileDialog1.ShowDialog();
            if (result == true)
            {
                try
                {
                    string filePath = openFileDialog1.FileName;
                    txt_fileDesign1.Text = filePath;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }
        public int ConvertToJson(Object _object, string name)
        {
            string savePath = $"{Const.ModelPath}";
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            string configPath = $"{savePath}/{name}.json";
            string js = JsonConvert.SerializeObject(_object);
            File.WriteAllText(configPath, js);
            return 0;
        }
        private void bt_selectFile2(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            Nullable<bool> result = openFileDialog1.ShowDialog();
            if (result == true)
            {
                try
                {
                    string filePath = openFileDialog1.FileName;
                    txt_fileDesign2.Text = filePath;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            _Param.Save();
        }

        private void txt_portLight_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.LIGHT_PORT = t.Text;
        }

        private void txt_ipPlc1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.IP_PLC_1 = t.Text;
        }

        private void txt_portPlc1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PORT_PLC_1 = Convert.ToInt32(t.Text);
        }

        private void txt_ipPlc2_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.IP_PLC_2 = t.Text;
        }

        private void txt_portPlc2_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PORT_PLC_2 = Convert.ToInt32(t.Text);
        }

        private void txt_ipPrinter1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.IP_PRINTER_1 = t.Text;
        }

        private void txt_fileDesign1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PATH_DESIGN_CODE_2D = t.Text;
        }

        private void txt_ipPrinter2_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.IP_PRINTER_2 = t.Text;
        }

        private void txt_fileDesign2_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PATH_DESIGN_CODE_1D = t.Text;
        }

        private void txt_pixelToMm1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PIXEL_MM_C1 =Convert.ToDouble(t.Text);
        }

        private void txt_pixelToMm2_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PIXEL_MM_C2 =Convert.ToDouble(t.Text);
        }

        private void txt_pixelToMm3_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.PIXEL_MM_C3 =Convert.ToDouble(t.Text);
        }

        private void txt_mmToPulse_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.MM_PULSE = Convert.ToDouble(t.Text);
        }

        private void txt_angleToPulse_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.ANGLE_PULSE = Convert.ToDouble(t.Text);
        }

        private void txt_portScaner_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.SCANNER_PORT = t.Text;
        }

        private void txt_tolerantXY_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.TOLERANCE =Convert.ToDouble(t.Text);
        }

        private void txt_toleranceAngle_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox t = sender as TextBox;
            _Param.ANGLE_TOLERANCE = Convert.ToDouble(t.Text);
        }
    }
}
