﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Model
{
    public class AOI
    {
        public int CH { get; set; }
        //public int CH2 { get; set; }
        //public int CH3 { get; set; }
        //public int CH4 { get; set; }
        public int ExposureTime { get; set; }
        public double Gamma { get; set; }
        public AOI()
        {
            this.ExposureTime = 5000;
            this.Gamma = 1;
            this.CH = 127;
            //this.CH2 = 127;
            //this.CH3 = 127;
            //this.CH4 = 127;
        }
        public AOI Copy()
        {
            AOI aoi = new AOI();
            aoi.CH = this.CH;
            //aoi.CH2 = this.CH2;
            //aoi.CH3 = this.CH3;
            //aoi.CH4 = this.CH4;
            aoi.ExposureTime = this.ExposureTime;
            aoi.Gamma = this.Gamma;
            return aoi;
        }
    }
}
