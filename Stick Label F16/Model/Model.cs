﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stick_Label_F16.Algorithm.Properties;
namespace Stick_Label_F16.Model
{
    public class Model
    {
        public string ID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string ModelType { get; set; }
        public List<ObjectLocation> Labels { get; set; }
        public List<bool> ListEnable { get; set; }
        public Model(string ModelType)
        {
            this.ID = Guid.NewGuid().ToString().ToUpper();
            this.Created = DateTime.Now;
            this.Modified = DateTime.Now;
            this.ModelType = ModelType;
            this.Labels = new List<ObjectLocation>();
            this.ListEnable = new List<bool>(8) { };
            for (int i= 0; i < 8; i++)
            {
                this.Labels.Add(new ObjectLocation());
                ListEnable.Add(true);
            }
                
        }
        public int Save()
        {
            this.Modified = DateTime.Now;
            string savePath = $"{Const.ModelPath}/{this.ModelType}";
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            string configPath = $"{savePath}/{this.ModelType}.json";
            string js = JsonConvert.SerializeObject(this);
            File.WriteAllText(configPath, js);
            return 0;
        }
        public static Model LoadModel(string ModelName)
        {

            string path = $"{Const.ModelPath}/{ModelName}/{ModelName}.json";
            if (File.Exists(path))
            {
                string s = File.ReadAllText(path);
                Model model = null;
                try
                {
                    model = JsonConvert.DeserializeObject<Model>(s,new ObjectConverter());
                    return model;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
                //if (model != null)
                //{

                //    return model;
                //}
                //else
                //    return null;
            }
            else
                return null;
        }

        public static string[] GetModelNames()
        {
            if (!Directory.Exists(Const.ModelPath))
            {
                Directory.CreateDirectory(Const.ModelPath);
            }
            string[] mListModelNames = Directory.GetDirectories(Const.ModelPath);
            for (int i = 0; i < mListModelNames.Length; i++)
            {
                DirectoryInfo dir = new DirectoryInfo(mListModelNames[i]);
                mListModelNames[i] = dir.Name;
            }
            return mListModelNames;
        }
        public void Delete()
        {
            Directory.Delete(Const.ModelPath + "//" + this.ModelType, true);
        }
    }
}
