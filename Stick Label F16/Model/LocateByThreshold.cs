﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace Stick_Label_F16.Model
{
    public class LocateByThreshold
    {
        public int LabelType { get; set; }
        public int Thresh { get; set; }
        public Point MarkPoint { get; set; }
        public double Radius { get; set; }
        public AOI AOI { get; set; }
        public LocateByThreshold()
        {
            this.Thresh = 127;
            this.Radius = 0;
            this.AOI = new AOI();
        }
        public LocateByThreshold Copy()
        {
            LocateByThreshold Locate = new LocateByThreshold();
            Locate.Thresh = this.Thresh;
            Locate.MarkPoint = this.MarkPoint;
            Locate.Radius = this.Radius;
            Locate.AOI = this.AOI;
            return Locate;
        }
    }
}
