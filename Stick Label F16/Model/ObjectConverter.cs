﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stick_Label_F16.Algorithm.Properties;
namespace Stick_Label_F16.Model
{
    public class ObjectConverter:JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(IProperty).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader,Type objectType, object existingValue, JsonSerializer serializer)
        {
            var check = reader.TokenType;


            if (reader != null)
            {
                JObject jo = JObject.Load(reader);
                string className = (string)jo["FullName"];
                Type type = Type.GetType(className);
                IProperty item = (IProperty)Activator.CreateInstance(type);
                serializer.Populate(jo.CreateReader(), item);
                return item;
            }
            else
                return null;

        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
