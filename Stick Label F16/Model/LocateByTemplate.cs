﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace Stick_Label_F16.Model
{
    public class LocateByTemplate
    {
        public string PathImageTemplate { get; set; }
        public double ScoreMatching { get; set; }
        public Rectangle ROI { get;set; }
        public int SearchX { get; set; }
        public int SearchY { get; set; }
        public LocateByTemplate()
        {
            this.ScoreMatching = 70;
            this.SearchX = 3;
            this.SearchY = 3;
        }
        public LocateByTemplate Copy()
        {
            LocateByTemplate locate = new LocateByTemplate();
            locate.ScoreMatching = this.ScoreMatching;
            locate.ROI = this.ROI;
            locate.SearchX = this.SearchX;
            locate.SearchY = this.SearchY;
            return locate;
        }
    }
}
