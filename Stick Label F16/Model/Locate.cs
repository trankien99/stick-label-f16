﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Stick_Label_F16.Algorithm.Properties;
namespace Stick_Label_F16.Model
{
    public class Locate
    {
        
        public string NameAlgorithmLocating { get; set; }
        public int IdAlgorithm { get; set; }
        public double Angle { get; set; }
        public Point MarkPoint { get; set; }
        public AOI AOI { get; set; }
        public IProperty Property { get; set; }
        public Locate()
        {
            this.Angle = 0;
            this.IdAlgorithm = 1;
            this.AOI = new AOI();
            Property = new Threshold();
        }
        public Locate Copy()
        {
            Locate label = new Locate();
            label.NameAlgorithmLocating = this.NameAlgorithmLocating;
            label.IdAlgorithm = this.IdAlgorithm;
            label.Angle = this.Angle;
            label.AOI = this.AOI;
            label.MarkPoint = this.MarkPoint;
            label.Property = this.Property.Copy();
            return label;
        }
    }
}
