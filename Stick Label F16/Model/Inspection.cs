﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Stick_Label_F16.Algorithm.Properties;
namespace Stick_Label_F16.Model
{
    public class Inspection
    {
        public string NameAlgorithm { get; set; }
        public int IdAlgorithm { get; set; }
        public AOI AOI { get; set; }
        public int QtyLabels { get; set; }
        public List<IProperty> Property { get; set; }
        public Inspection()
        {
            this.IdAlgorithm = 1;
            this.AOI = new AOI();
            this.QtyLabels = 1;
            Property = new List<IProperty>() { };
            //for(int i = 0; i < 1; i++)
            //{
            //    Property.Add(new Scratch());
            //}
        }
        public Inspection Copy()
        {
            Inspection inspection = new Inspection();
            inspection.AOI = this.AOI;
            inspection.IdAlgorithm = this.IdAlgorithm;
            inspection.NameAlgorithm = this.NameAlgorithm;
            inspection.Property = this.Property;
            return inspection;
        }
    }
}
