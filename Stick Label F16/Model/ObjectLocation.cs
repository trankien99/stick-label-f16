﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Model
{
    public class ObjectLocation
    {
        public Locate Locate { get; set; }
        public Inspection Inspection { get; set; }
        public ObjectLocation()
        {
            this.Locate = new Locate();
            this.Inspection = new Inspection();
        }
        public ObjectLocation Copy()
        {
            ObjectLocation object1 = new ObjectLocation();
            object1.Locate = this.Locate;
            object1.Inspection = this.Inspection;
            return object1;
        }
    }
}
