﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Database
{
    public class PingTime
    {
        private Properties.Settings _Param = Properties.Settings.Default;
        public string machine_name { get; set; } 
        public string ip_machine { get; set; }
        public int id_machine { get; set; }
        public PingTime()
        {
            machine_name = _Param.NAME;
            ip_machine = _Param.IP;
            id_machine = _Param.ID_MACHINE;
        }
    }
}
