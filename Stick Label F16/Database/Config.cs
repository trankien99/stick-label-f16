﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SticklabelContext;
namespace Stick_Label_F16.Database
{
    public class Config
    {
        private string _connectString = System.Configuration.ConfigurationManager.ConnectionStrings["SticklabelDataContextConnectionString"].ToString();
        private Properties.Settings _Param = Properties.Settings.Default;
        public string machine_name { get; set; }
        public string machine_type { get; set; }
        public string Process { get; set; }
        public int index { get; set; }
        public int id_machine { get; set; }
        public List<InforModel> model_configurations { get; set; }
        public Config()
        {
            machine_name = _Param.NAME;
            machine_type = _Param.TYPE;
            Process = _Param.LINE;
            index = _Param.INDEX;
            id_machine = _Param.ID_MACHINE;
            model_configurations = getInforModel();
            
        }
        private List<InforModel> getInforModel()
        {
            using (SticklabelDataContext db = new SticklabelDataContext(_connectString))
            {
                List<InforModel> infors = new List<InforModel>();
                var query = from Model in db.Models
                            select Model ;
                foreach (SticklabelContext.Model values in query)
                {
                    InforModel inforModel = new InforModel(values.Name);
                    inforModel.UPH = Convert.ToDouble(values.Uph);
                    inforModel.cycle_time = Convert.ToDouble(values.CycleTime);
                    var query2 = from Carrier in db.Carriers
                                select Carrier.QtyProduct;
                    foreach(int qtyProduct in query2)
                    {
                        inforModel.quantity_on_carrier = qtyProduct;
                    }
                    infors.Add(inforModel);
                }
                return infors;
            }
        }

    }
    public class InforModel
    {
        private string _connectString = System.Configuration.ConfigurationManager.ConnectionStrings["SticklabelDataContextConnectionString"].ToString();
        public string name { get; set; }
        public double UPH { get; set; }
        public double cycle_time { get; set; }
        public int quantity_on_carrier { get; set; }
        public List<StepConfig> step_configurations { get; set; }
        public InforModel(string modelName)
        {
            name = modelName;
            step_configurations = GetStepConfigs();
        }
        public List<StepConfig> GetStepConfigs()
        {
            using (SticklabelDataContext db = new SticklabelDataContext(_connectString))
            {
                List<StepConfig> stepConfigs = new List<StepConfig>();
                var query = from Step in db.Steps join model in db.Models on Step.ModelId equals model.Id where model.Name==this.name 
                            select Step;
                int count = 0;
                for(int i = 0; i < 4; i++)
                {
                    StepConfig stepConfig = new StepConfig();
                    foreach (Step values in query)
                    {
                        switch (i)
                        {
                            case 0:
                                stepConfig.name = "Load";
                                stepConfig.cycle_time = Convert.ToDouble(values.Load);
                                break;
                            case 1:
                                stepConfig.name = "Positioning";
                                stepConfig.cycle_time = Convert.ToDouble(values.Positioning);
                                break;
                            case 2:
                                stepConfig.name = "Stick";
                                stepConfig.cycle_time = Convert.ToDouble(values.Stick);
                                break;
                            case 3:
                                stepConfig.name = "Unload";
                                stepConfig.cycle_time = Convert.ToDouble(values.Unload);
                                break;
                        }
                    }
                    stepConfigs.Add(stepConfig);
                }
               
                return stepConfigs;
            }
        }

    }
    public struct StepConfig
    {
        public string name { get; set; }
        public double cycle_time { get; set; }

    }

}
