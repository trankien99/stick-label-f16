﻿using SticklabelContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stick_Label_F16.Database
{
    public class RawData
    {
        private Properties.Settings _Param = Properties.Settings.Default;
        public int id_machine { get; set; }
        public string machine_name { get; set; }
        public string model { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public List<string> images { get; set; }
        public string carrier_code { get; set; }
        public List<ProductResult> product_results { get; set; }
        public List<StepResult> step_results { get; set; }
        public MachineParam machine_parameters { get; set; }
        public RawData(string modelName)
        {
            id_machine = _Param.ID_MACHINE;
            machine_name = _Param.NAME;
            model = modelName;
            step_results = SetStep();
            product_results = new List<ProductResult>();
            product_results.Add(new ProductResult(model));
        }
        private List<StepResult> SetStep()
        {
            List<StepResult> stepRes = new List<StepResult>();
            for (int i = 0; i < 4; i++)
            {
                StepResult stepResult = new StepResult();
                switch (i)
                {
                    case 0:
                        stepResult.name = "load";
                        stepRes.Add(stepResult);
                        break;
                    case 1:
                        stepResult.name = "positioning";
                        stepRes.Add(stepResult);
                        break;
                    case 2:
                        stepResult.name = "stick";
                        stepRes.Add(stepResult);
                        break;
                    case 3:
                        stepResult.name = "unload";
                        stepRes.Add(stepResult);
                        break;
                }
            }
            return stepRes;
        }
       
    }
    public struct StepResult
    {
        public string name { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
    }
    public class ProductResult
    {
        public string status { get; set; }
        public string SN { get; set; }
        public List<LocationResult> location_results { get; set; }
        public ProductResult(string modelName)
        {
            location_results = new List<LocationResult>();
            location_results.Add(new LocationResult(modelName));
        }
    }
    public class LocationResult
    {
        private string _connectString = System.Configuration.ConfigurationManager.ConnectionStrings["SticklabelDataContextConnectionString"].ToString();
        public string name { get; set; }
        public List<SubLocation> sub_locations { get; set; }
        public LocationResult(string modelName)
        {
            name = "Product1";
            sub_locations = getLocationResult(modelName);
        }
        private List<SubLocation> getLocationResult(string modelName)
        {
            using (SticklabelDataContext db = new SticklabelDataContext(_connectString))
            {
                List<SubLocation> Listlocation = new List<SubLocation>();
                var query = from Label in db.Labels
                            join product in db.Products on Label.ProductId equals product.Id where product.StartTime>Convert.ToDateTime("2022-02-25 09:00:00")
                            join carrier in db.Carriers on product.CarrierId equals carrier.Id
                            join model in db.Models on carrier.Id equals model.CarrierId
                            where model.Name == modelName
                            select Label;
                foreach (SticklabelContext.Label values in query)
                {
                    SubLocation location = new SubLocation();
                    location.index = Convert.ToInt32(values.Index);
                    location.status = values.Status;
                    location.error_code = values.ErrorCode;
                    Listlocation.Add(location);
                }
                return Listlocation;
            }
        }
    }
    public struct SubLocation
    {
        public int index { get; set; }
        public string status { get; set; }
        public string error_code { get; set; }
    }
    public struct MachineParam { }
}
